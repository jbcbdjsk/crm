declare module "mysql2/promise" {
    namespace MySQL2Promise {
        function createPool(options: MySQLConfig): MySQLPool;
        function createConnection(options: MySQLConfig): MySQL2Connection;
        class MySQLField {
            type: string;
            length: number;
            buffer(): any;
            string(): string;
            geometry(): any;
        }
        class MySQLConfig {
            debug?: boolean;
            host: string;
            user: string;
            database: string;
            password: string;
            connectionLimit: number;
            typeCast: (field: MySQLField, useDefaultTypeCasting: () => any) => any;
        }
        class MySQLPool {
            getConnection(): Promise<MySQL2Connection>;
        }
        class MySQL2Connection {
            /**
             * The promise resolves two values: 
             * The first is either: a result set - an array of objects OR a ResultSetHeader with fieldcount, affectedRows, insertId, info, serverSTatus, and warningStatus
             */
            query(query: string, params?: string[]): Promise<[any, any[]]>
            /**
             * Use the connection to escape a value in a sql string
             */
            escape(param: any): string;
            release(): void;
        }
    }  
    export = MySQL2Promise;
}