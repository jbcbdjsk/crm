--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1 (Debian 11.1-1.pgdg90+1)
-- Dumped by pg_dump version 11.1 (Debian 11.1-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: organization; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.organization (id, keyuser, name, parentorganizationid) FROM stdin;
1	demo	Demo	\N
2	child	Demo Child	1
3	grandchild	Demo Grandchild	2
4	acme	Acme Store	\N
\.


--
-- Data for Name: customer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customer (id, organizationid, keyuser, namelast, namefirst, namebusiness, isbusiness, datecreated, datemodified, isdeleted) FROM stdin;
1	1	BTHOMSON	Bob	Thomson		f	2018-06-04 00:46:09+00	2018-06-04 01:21:36+00	f
43	1	MYUSER1531704103199	Customer Updated	Demo		f	2018-07-16 06:21:53+00	2018-07-16 06:22:09+00	f
44	1	MYUSER1531704233397	Customer Updated	Demo		f	2018-07-16 06:24:04+00	2018-07-16 06:24:19+00	f
45	1	MYUSER1531704321664	Customer Updated	Demo		f	2018-07-16 06:25:31+00	2018-07-16 06:25:47+00	f
46	1	MYUSER1531704413305	Customer Updated	Demo		f	2018-07-16 06:27:03+00	2018-07-16 06:27:20+00	f
47	1	MYUSER1531704560323	Customer Updated	Demo		f	2018-07-16 06:29:29+00	2018-07-16 06:29:45+00	f
48	1	MYUSER1531704644220	Customer Updated	Demo		f	2018-07-16 06:30:51+00	2018-07-16 06:31:07+00	f
49	1	MYUSER1531704841389	Customer Updated	Demo		f	2018-07-16 06:34:11+00	2018-07-16 06:34:27+00	f
50	1	MYUSER1531705027699	Customer Updated	Demo		f	2018-07-16 06:37:21+00	2018-07-16 06:37:36+00	f
51	1	MYUSER1531705272499	Customer Updated	Demo		f	2018-07-16 06:41:23+00	2018-07-16 06:41:38+00	f
52	1	MYUSER1531705386006	Customer Updated	Demo		f	2018-07-16 06:43:19+00	2018-07-16 06:43:35+00	f
53	1	MYUSER1531705606168	Customer Updated	Demo		f	2018-07-16 06:46:59+00	2018-07-16 06:47:15+00	f
54	1	MYUSER1531705656005	Customer Updated	Demo		f	2018-07-16 06:48:13+00	2018-07-16 06:48:30+00	f
55	1	MYUSER1531705841704	Customer Updated	Demo		f	2018-07-16 06:50:51+00	2018-07-16 06:51:07+00	f
56	1	MYUSER1531705994308	Customer Updated	Demo		f	2018-07-16 06:53:24+00	2018-07-16 06:53:41+00	f
57	1	MYUSER1531706088556	Customer Updated	Demo		f	2018-07-16 06:54:59+00	2018-07-16 06:55:14+00	f
58	1	MYUSER1531706392864	Customer Updated	Demo		f	2018-07-16 07:00:03+00	2018-07-16 07:00:18+00	f
59	1	MYUSER1531709489001	Customer Updated	Demo		f	2018-07-16 07:51:40+00	2018-07-16 07:51:55+00	f
60	1	MYUSER1535074774617	Customer Updated	Demo		f	2018-08-24 06:39:46+00	2018-08-24 06:40:02+00	f
61	1	MYUSER1535078861555	Customer Updated	Demo		f	2018-08-24 07:47:55+00	2018-08-24 07:48:18+00	t
6	1	MYUSER1537144593100	Customer	Demo		f	2018-09-17 00:36:33.306754+00	2018-09-17 00:36:33.306754+00	f
7	1	MYUSER1537144797564	Customer	Demo		f	2018-09-17 00:39:57.772586+00	2018-09-17 00:39:57.772586+00	f
8	1	MYUSER1537144984812	Customer	Demo		f	2018-09-17 00:43:05.017029+00	2018-09-17 00:43:05.017029+00	f
9	1	MYUSER1537145006925	Customer	Demo		f	2018-09-17 00:43:27.111276+00	2018-09-17 00:43:27.111276+00	f
10	1	MYUSER1537145112180	Customer	Demo		f	2018-09-17 00:45:12.392972+00	2018-09-17 00:45:12.392972+00	f
11	1	MYUSER1537145132589	Customer	Demo		f	2018-09-17 00:45:32.800644+00	2018-09-17 00:45:32.800644+00	f
12	1	MYUSER1537145794638	Customer	Demo		f	2018-09-17 00:56:34.847686+00	2018-09-17 00:56:34.847686+00	f
13	1	MYUSER1537145878459	Customer	Demo		f	2018-09-17 00:57:58.662532+00	2018-09-17 00:57:58.662532+00	f
14	1	MYUSER1537145909936	Customer	Demo		f	2018-09-17 00:58:30.153518+00	2018-09-17 00:58:30.153518+00	f
15	1	MYUSER1537145951431	Customer	Demo		f	2018-09-17 00:59:11.640055+00	2018-09-17 00:59:11.640055+00	f
16	1	MYUSER1537145989544	Customer	Demo		f	2018-09-17 00:59:49.756465+00	2018-09-17 00:59:49.756465+00	f
17	1	MYUSER1537146010423	Customer	Demo		f	2018-09-17 01:00:10.628215+00	2018-09-17 01:00:10.628215+00	f
18	1	MYUSER1537146178589	Customer	Demo		f	2018-09-17 01:02:58.79015+00	2018-09-17 01:02:58.79015+00	f
19	1	MYUSER1537146218708	Customer	Demo		f	2018-09-17 01:03:38.909473+00	2018-09-17 01:03:38.909473+00	f
20	1	MYUSER1537146652608	Customer	Demo		f	2018-09-17 01:10:52.81466+00	2018-09-17 01:10:52.81466+00	f
28	1	MYUSER1537148053465	Customer Updated	Demo		f	2018-09-17 01:34:13.670126+00	2018-09-17 01:34:13.786444+00	t
21	1	MYUSER1537146661172	Customer	Demo		f	2018-09-17 01:11:01.3473+00	2018-09-17 01:11:01.3473+00	f
29	1	MYUSER1537148058044	Customer Updated	Demo		f	2018-09-17 01:34:18.223564+00	2018-09-17 01:34:18.336321+00	t
22	1	MYUSER1537146726749	Customer	Demo		f	2018-09-17 01:12:06.951549+00	2018-09-17 01:12:06.951549+00	f
23	1	MYUSER1537146732341	Customer	Demo		f	2018-09-17 01:12:12.519108+00	2018-09-17 01:12:12.519108+00	f
24	1	MYUSER1537146868991	Customer Updated	Demo		f	2018-09-17 01:14:29.191979+00	2018-09-17 01:14:29.319935+00	t
25	1	MYUSER1537146875159	Customer Updated	Demo		f	2018-09-17 01:14:35.337894+00	2018-09-17 01:14:35.454+00	t
26	1	MYUSER1537147125368	Customer Updated	Demo		f	2018-09-17 01:18:45.573644+00	2018-09-17 01:18:45.701188+00	t
27	1	MYUSER1537147171144	Customer Updated	Demo		f	2018-09-17 01:19:31.352231+00	2018-09-17 01:19:31.481051+00	t
\.


--
-- Data for Name: customeraddress; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customeraddress (customerid, keysort, street1, street2, city, state, postalcode, isprimary, isphysical, ismailing) FROM stdin;
1	1	123 Main st	c/o Some Guy	Jonesboro	AR	72401	t	f	f
43	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
43	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
44	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
44	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
45	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
45	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
46	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
46	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
47	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
47	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
48	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
48	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
49	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
49	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
50	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
50	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
51	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
51	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
52	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
52	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
53	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
53	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
54	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
54	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
55	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
55	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
56	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
56	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
57	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
57	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
58	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
58	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
59	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
59	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
60	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
60	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
61	1	123 Test Script St	Updated line	Jonesboro	AR	72401	t	t	t
61	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
6	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
6	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
7	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
7	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
8	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
8	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
9	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
9	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
10	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
10	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
11	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
11	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
12	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
12	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
13	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
13	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
14	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
14	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
15	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
15	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
16	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
16	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
17	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
17	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
18	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
18	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
19	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
19	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
20	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
20	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
21	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
21	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
22	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
22	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
23	1	123 Test Script St		Jonesboro	AR	72401	t	t	t
23	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
24	1	123 Test Script St	Updated line	Jonesboro	AR	72401	f	f	t
24	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
25	1	123 Test Script St	Updated line	Jonesboro	AR	72401	f	f	t
25	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
26	1	123 Test Script St	Updated line	Jonesboro	AR	72401	f	f	t
26	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
27	1	123 Test Script St	Updated line	Jonesboro	AR	72401	f	f	t
27	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
28	1	123 Test Script St	Updated line	Jonesboro	AR	72401	f	f	t
28	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
29	1	123 Test Script St	Updated line	Jonesboro	AR	72401	f	f	t
29	2	456 Test Script Mailing St		Jonesboro	AR	72401	f	f	t
\.


--
-- Data for Name: customeremailaddress; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customeremailaddress (customerid, keysort, emailaddress, isprimary) FROM stdin;
1	1	testperson@example.com	t
43	1	testscript1@example.com	t
43	2	testscript2@example.com	f
44	1	testscript1@example.com	t
44	2	testscript2@example.com	f
45	1	testscript1@example.com	t
45	2	testscript2@example.com	f
46	1	testscript1@example.com	t
46	2	testscript2@example.com	f
47	1	testscript1@example.com	t
47	2	testscript2@example.com	f
48	1	testscript1@example.com	t
48	2	testscript2@example.com	f
49	1	testscript1@example.com	t
49	2	testscript2@example.com	f
50	1	testscript1@example.com	t
50	2	testscript2@example.com	f
51	1	testscript1@example.com	t
51	2	testscript2@example.com	f
52	1	testscript1@example.com	t
52	2	testscript2@example.com	f
53	1	testscript1@example.com	t
53	2	testscript2@example.com	f
54	1	testscript1@example.com	t
54	2	testscript2@example.com	f
55	1	testscript1@example.com	t
55	2	testscript2@example.com	f
56	1	testscript1@example.com	t
56	2	testscript2@example.com	f
57	1	testscript1@example.com	t
57	2	testscript2@example.com	f
58	1	testscript1@example.com	t
58	2	testscript2@example.com	f
59	1	testscript1@example.com	t
59	2	testscript2@example.com	f
60	1	testscript1@example.com	t
60	2	testscript2@example.com	f
61	1	testscript1@example.com	t
61	2	testscript2@example.com	f
6	1	testscript1@example.com	t
6	2	testscript2@example.com	f
7	1	testscript1@example.com	t
7	2	testscript2@example.com	f
8	1	testscript1@example.com	t
8	2	testscript2@example.com	f
9	1	testscript1@example.com	t
9	2	testscript2@example.com	f
10	1	testscript1@example.com	t
10	2	testscript2@example.com	f
11	1	testscript1@example.com	t
11	2	testscript2@example.com	f
12	1	testscript1@example.com	t
12	2	testscript2@example.com	f
13	1	testscript1@example.com	t
13	2	testscript2@example.com	f
14	1	testscript1@example.com	t
14	2	testscript2@example.com	f
15	1	testscript1@example.com	t
15	2	testscript2@example.com	f
16	1	testscript1@example.com	t
16	2	testscript2@example.com	f
17	1	testscript1@example.com	t
17	2	testscript2@example.com	f
18	1	testscript1@example.com	t
18	2	testscript2@example.com	f
19	1	testscript1@example.com	t
19	2	testscript2@example.com	f
20	1	testscript1@example.com	t
20	2	testscript2@example.com	f
21	1	testscript1@example.com	t
21	2	testscript2@example.com	f
22	1	testscript1@example.com	t
22	2	testscript2@example.com	f
23	1	testscript1@example.com	t
23	2	testscript2@example.com	f
29	1	testscript1@example.com	t
29	2	testscript2@example.com	f
24	1	testscript1@example.com	t
24	2	testscript2@example.com	f
25	1	testscript1@example.com	t
25	2	testscript2@example.com	f
26	1	testscript1@example.com	t
26	2	testscript2@example.com	f
27	1	testscript1@example.com	t
27	2	testscript2@example.com	f
28	1	testscript1@example.com	t
28	2	testscript2@example.com	f
\.


--
-- Data for Name: customerphone; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.customerphone (customerid, keysort, phonenumber, phonetype, isprimary) FROM stdin;
1	1	8708675309	Mobile	t
43	1	8700000001	Home	t
43	2	8700000002	Mobile	f
44	1	8700000001	Home	t
44	2	8700000002	Mobile	f
45	1	8700000001	Home	t
45	2	8700000002	Mobile	f
46	1	8700000001	Home	t
46	2	8700000002	Mobile	f
47	1	8700000001	Home	t
47	2	8700000002	Mobile	f
48	1	8700000001	Home	t
48	2	8700000002	Mobile	f
49	1	8700000001	Home	t
49	2	8700000002	Mobile	f
50	1	8700000001	Home	t
50	2	8700000002	Mobile	f
51	1	8700000001	Home	t
51	2	8700000002	Mobile	f
52	1	8700000001	Home	t
52	2	8700000002	Mobile	f
53	1	8700000001	Home	t
53	2	8700000002	Mobile	f
54	1	8700000001	Home	t
54	2	8700000002	Mobile	f
55	1	8700000001	Home	t
55	2	8700000002	Mobile	f
56	1	8700000001	Home	t
56	2	8700000002	Mobile	f
57	1	8700000001	Home	t
57	2	8700000002	Mobile	f
58	1	8700000001	Home	t
58	2	8700000002	Mobile	f
59	1	8700000001	Home	t
59	2	8700000002	Mobile	f
60	1	8700000001	Home	t
60	2	8700000002	Mobile	f
61	1	8700000001	Home	t
61	2	8700000002	Mobile	f
6	1	8700000001	Home	t
6	2	8700000002	Mobile	f
7	1	8700000001	Home	t
7	2	8700000002	Mobile	f
8	1	8700000001	Home	t
8	2	8700000002	Mobile	f
9	1	8700000001	Home	t
9	2	8700000002	Mobile	f
10	1	8700000001	Home	t
10	2	8700000002	Mobile	f
11	1	8700000001	Home	t
11	2	8700000002	Mobile	f
12	1	8700000001	Home	t
12	2	8700000002	Mobile	f
13	1	8700000001	Home	t
13	2	8700000002	Mobile	f
14	1	8700000001	Home	t
14	2	8700000002	Mobile	f
15	1	8700000001	Home	t
15	2	8700000002	Mobile	f
16	1	8700000001	Home	t
16	2	8700000002	Mobile	f
17	1	8700000001	Home	t
17	2	8700000002	Mobile	f
18	1	8700000001	Home	t
18	2	8700000002	Mobile	f
19	1	8700000001	Home	t
19	2	8700000002	Mobile	f
20	1	8700000001	Home	t
20	2	8700000002	Mobile	f
21	1	8700000001	Home	t
21	2	8700000002	Mobile	f
22	1	8700000001	Home	t
22	2	8700000002	Mobile	f
23	1	8700000001	Home	t
23	2	8700000002	Mobile	f
24	1	8700000001	Home	t
24	2	8700000002	Mobile	f
25	1	8700000001	Home	t
25	2	8700000002	Mobile	f
26	1	8700000001	Home	t
26	2	8700000002	Mobile	f
27	1	8700000001	Home	t
27	2	8700000002	Mobile	f
28	1	8700000001	Home	t
28	2	8700000002	Mobile	f
29	1	8700000001	Home	t
29	2	8700000002	Mobile	f
\.


--
-- Data for Name: orguser; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orguser (id, organizationid, namefirst, namelast, namelogin, passwordhash) FROM stdin;
1	1	Demo	User	demo	$2b$10$DrgtiQBviybxpXCY35M0huYpFGGkF69.s6XuhlSiXUnbxOuEoqS.O
\.

--
-- Data for Name: systemuser; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.systemuser (id, name, namelogin, passwordhash) FROM stdin;
1	Admin	admin	$2b$10$DrgtiQBviybxpXCY35M0huYpFGGkF69.s6XuhlSiXUnbxOuEoqS.O
\.


--
-- Name: customer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.customer_id_seq', 29, true);


--
-- Name: organization_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.organization_id_seq', 4, true);


--
-- Name: orguser_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orguser_id_seq', 1, false);


--
-- Name: systemuser_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.systemuser_id_seq', 1, false);


--
-- PostgreSQL database dump complete
--

