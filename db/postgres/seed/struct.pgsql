--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1 (Debian 11.1-1.pgdg90+1)
-- Dumped by pg_dump version 11.2 (Ubuntu 11.2-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

ALTER TABLE ONLY public.productsubscriptioninfo DROP CONSTRAINT productsubscriptioninfo_product_fk;
ALTER TABLE ONLY public.productquantityinfo DROP CONSTRAINT productquantityinfo_product_fk;
ALTER TABLE ONLY public.product DROP CONSTRAINT product_organization_fk;
ALTER TABLE ONLY public.organization DROP CONSTRAINT parentorganizationid;
ALTER TABLE ONLY public.orguser DROP CONSTRAINT orguser_organization_fk;
ALTER TABLE ONLY public.customerphone DROP CONSTRAINT customerphone_customerid;
ALTER TABLE ONLY public.customeremailaddress DROP CONSTRAINT customeremailaddress_customerid;
ALTER TABLE ONLY public.customeraddress DROP CONSTRAINT customeraddress_customerid;
ALTER TABLE ONLY public.customer DROP CONSTRAINT customer_parentcustomerid_fk;
ALTER TABLE ONLY public.customer DROP CONSTRAINT customer_organizationid;
ALTER TABLE ONLY public.customer DROP CONSTRAINT useruniquekey;
ALTER TABLE ONLY public.systemuser DROP CONSTRAINT systemuser_pkey;
ALTER TABLE ONLY public.session DROP CONSTRAINT session_pkey;
ALTER TABLE ONLY public.product DROP CONSTRAINT product_pk;
ALTER TABLE ONLY public.orguser DROP CONSTRAINT orguser_pkey;
ALTER TABLE ONLY public.orguser DROP CONSTRAINT organizationid_namelogin;
ALTER TABLE ONLY public.organization DROP CONSTRAINT organization_pkey;
ALTER TABLE ONLY public.organization DROP CONSTRAINT keyuser_unique;
ALTER TABLE ONLY public.customerphone DROP CONSTRAINT customerphone_pk;
ALTER TABLE ONLY public.customeremailaddress DROP CONSTRAINT customeremailaddress_pk;
ALTER TABLE ONLY public.customeraddress DROP CONSTRAINT customeraddress_pk;
ALTER TABLE ONLY public.customer DROP CONSTRAINT customer_pkey;
ALTER TABLE public.systemuser ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.product ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.orguser ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.organization ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.customer ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.systemuser_id_seq;
DROP TABLE public.systemuser;
DROP TABLE public.session;
DROP TABLE public.productsubscriptioninfo;
DROP TABLE public.productquantityinfo;
DROP SEQUENCE public.product_id_seq;
DROP TABLE public.product;
DROP SEQUENCE public.orguser_id_seq;
DROP TABLE public.orguser;
DROP SEQUENCE public.organization_id_seq;
DROP TABLE public.organization;
DROP TABLE public.customerphone;
DROP TABLE public.customeremailaddress;
DROP TABLE public.customeraddress;
DROP SEQUENCE public.customer_id_seq;
DROP TABLE public.customer;
SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customer (
    id bigint NOT NULL,
    organizationid bigint NOT NULL,
    keyuser character varying(50) NOT NULL,
    namelast character varying(50) NOT NULL,
    namefirst character varying(50) DEFAULT ''::character varying NOT NULL,
    namebusiness character varying(50) DEFAULT ''::character varying NOT NULL,
    isbusiness boolean NOT NULL,
    datecreated timestamp with time zone DEFAULT now() NOT NULL,
    datemodified timestamp with time zone DEFAULT now() NOT NULL,
    isdeleted boolean NOT NULL,
    parentcustomerid bigint
);


ALTER TABLE public.customer OWNER TO postgres;

--
-- Name: customer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.customer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customer_id_seq OWNER TO postgres;

--
-- Name: customer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.customer_id_seq OWNED BY public.customer.id;


--
-- Name: customeraddress; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customeraddress (
    customerid bigint NOT NULL,
    keysort integer NOT NULL,
    street1 character varying(100) NOT NULL,
    street2 character varying(100) NOT NULL,
    city character varying(100) NOT NULL,
    state character varying(50) NOT NULL,
    postalcode character varying(25) NOT NULL,
    isprimary boolean NOT NULL,
    isphysical boolean NOT NULL,
    ismailing boolean NOT NULL
);


ALTER TABLE public.customeraddress OWNER TO postgres;

--
-- Name: COLUMN customeraddress.keysort; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.customeraddress.keysort IS 'Start at 1';


--
-- Name: customeremailaddress; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customeremailaddress (
    customerid bigint NOT NULL,
    keysort integer NOT NULL,
    emailaddress character varying(100) NOT NULL,
    isprimary boolean NOT NULL
);


ALTER TABLE public.customeremailaddress OWNER TO postgres;

--
-- Name: COLUMN customeremailaddress.keysort; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.customeremailaddress.keysort IS 'Start at 1';


--
-- Name: COLUMN customeremailaddress.emailaddress; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.customeremailaddress.emailaddress IS 'Fully qualified email address';


--
-- Name: customerphone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.customerphone (
    customerid bigint NOT NULL,
    keysort integer NOT NULL,
    phonenumber character varying(20) NOT NULL,
    phonetype character varying(20) NOT NULL,
    isprimary boolean NOT NULL
);


ALTER TABLE public.customerphone OWNER TO postgres;

--
-- Name: COLUMN customerphone.phonetype; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.customerphone.phonetype IS 'Should be one value of an enumerated list such as Mobile, Home';


--
-- Name: organization; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.organization (
    id bigint NOT NULL,
    keyuser character varying(50) NOT NULL,
    name character varying(50) NOT NULL,
    parentorganizationid bigint
);


ALTER TABLE public.organization OWNER TO postgres;

--
-- Name: organization_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.organization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.organization_id_seq OWNER TO postgres;

--
-- Name: organization_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.organization_id_seq OWNED BY public.organization.id;


--
-- Name: orguser; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orguser (
    id bigint NOT NULL,
    organizationid bigint NOT NULL,
    namefirst character varying(50) NOT NULL,
    namelast character varying(50) NOT NULL,
    namelogin character varying(50),
    passwordhash character varying(100)
);


ALTER TABLE public.orguser OWNER TO postgres;

--
-- Name: orguser_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orguser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orguser_id_seq OWNER TO postgres;

--
-- Name: orguser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orguser_id_seq OWNED BY public.orguser.id;


--
-- Name: product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product (
    name character varying(50) NOT NULL,
    price integer NOT NULL,
    id bigint NOT NULL,
    organizationid bigint NOT NULL
);


ALTER TABLE public.product OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_id_seq OWNER TO postgres;

--
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_id_seq OWNED BY public.product.id;


--
-- Name: productquantityinfo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.productquantityinfo (
    productid bigint NOT NULL,
    costperunit integer NOT NULL,
    quantity integer NOT NULL,
    uom character varying(20) NOT NULL
);


ALTER TABLE public.productquantityinfo OWNER TO postgres;

--
-- Name: productsubscriptioninfo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.productsubscriptioninfo (
    productid bigint NOT NULL,
    duration numeric(10,2) NOT NULL,
    uom character varying(10) NOT NULL,
    renewalprice bigint NOT NULL
);


ALTER TABLE public.productsubscriptioninfo OWNER TO postgres;

--
-- Name: session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.session (
    sid character varying NOT NULL,
    sess json NOT NULL,
    expire timestamp(6) without time zone NOT NULL
);


ALTER TABLE public.session OWNER TO postgres;

--
-- Name: systemuser; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.systemuser (
    id bigint NOT NULL,
    name character varying(50) NOT NULL,
    namelogin character varying(50) NOT NULL,
    passwordhash character varying(100) NOT NULL
);


ALTER TABLE public.systemuser OWNER TO postgres;

--
-- Name: systemuser_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.systemuser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.systemuser_id_seq OWNER TO postgres;

--
-- Name: systemuser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.systemuser_id_seq OWNED BY public.systemuser.id;


--
-- Name: customer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer ALTER COLUMN id SET DEFAULT nextval('public.customer_id_seq'::regclass);


--
-- Name: organization id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organization ALTER COLUMN id SET DEFAULT nextval('public.organization_id_seq'::regclass);


--
-- Name: orguser id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orguser ALTER COLUMN id SET DEFAULT nextval('public.orguser_id_seq'::regclass);


--
-- Name: product id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product ALTER COLUMN id SET DEFAULT nextval('public.product_id_seq'::regclass);


--
-- Name: systemuser id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.systemuser ALTER COLUMN id SET DEFAULT nextval('public.systemuser_id_seq'::regclass);


--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id);


--
-- Name: customeraddress customeraddress_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customeraddress
    ADD CONSTRAINT customeraddress_pk PRIMARY KEY (customerid, keysort);


--
-- Name: customeremailaddress customeremailaddress_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customeremailaddress
    ADD CONSTRAINT customeremailaddress_pk PRIMARY KEY (customerid, keysort);


--
-- Name: customerphone customerphone_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customerphone
    ADD CONSTRAINT customerphone_pk PRIMARY KEY (customerid, keysort);


--
-- Name: organization keyuser_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organization
    ADD CONSTRAINT keyuser_unique UNIQUE (keyuser);


--
-- Name: organization organization_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organization
    ADD CONSTRAINT organization_pkey PRIMARY KEY (id);


--
-- Name: orguser organizationid_namelogin; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orguser
    ADD CONSTRAINT organizationid_namelogin UNIQUE (organizationid, namelogin);


--
-- Name: orguser orguser_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orguser
    ADD CONSTRAINT orguser_pkey PRIMARY KEY (id);


--
-- Name: product product_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pk PRIMARY KEY (id);


--
-- Name: session session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.session
    ADD CONSTRAINT session_pkey PRIMARY KEY (sid);


--
-- Name: systemuser systemuser_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.systemuser
    ADD CONSTRAINT systemuser_pkey PRIMARY KEY (id);


--
-- Name: customer useruniquekey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT useruniquekey UNIQUE (keyuser, organizationid);


--
-- Name: customer customer_organizationid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_organizationid FOREIGN KEY (organizationid) REFERENCES public.organization(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: customer customer_parentcustomerid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_parentcustomerid_fk FOREIGN KEY (parentcustomerid) REFERENCES public.customer(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: customeraddress customeraddress_customerid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customeraddress
    ADD CONSTRAINT customeraddress_customerid FOREIGN KEY (customerid) REFERENCES public.customer(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: customeremailaddress customeremailaddress_customerid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customeremailaddress
    ADD CONSTRAINT customeremailaddress_customerid FOREIGN KEY (customerid) REFERENCES public.customer(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: customerphone customerphone_customerid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.customerphone
    ADD CONSTRAINT customerphone_customerid FOREIGN KEY (customerid) REFERENCES public.customer(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: orguser orguser_organization_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orguser
    ADD CONSTRAINT orguser_organization_fk FOREIGN KEY (id) REFERENCES public.organization(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: organization parentorganizationid; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.organization
    ADD CONSTRAINT parentorganizationid FOREIGN KEY (parentorganizationid) REFERENCES public.organization(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- Name: product product_organization_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_organization_fk FOREIGN KEY (organizationid) REFERENCES public.organization(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: productquantityinfo productquantityinfo_product_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productquantityinfo
    ADD CONSTRAINT productquantityinfo_product_fk FOREIGN KEY (productid) REFERENCES public.product(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: productsubscriptioninfo productsubscriptioninfo_product_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productsubscriptioninfo
    ADD CONSTRAINT productsubscriptioninfo_product_fk FOREIGN KEY (productid) REFERENCES public.product(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

