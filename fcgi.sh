#!/bin/bash

trap "exit" INT TERM
trap "kill -- -\$CHILD_PGID 2> /dev/null" EXIT

NODE_PATH=/usr/lib/nodejs:/usr/lib/node_modules:/usr/share/javascript
export NODE_PATH 
cd "$(dirname "$0")"

set -m
./node_modules/.bin/ts-node ./src/server.ts <&0 >&1 2>&2 &
CHILD_PGID=$!
set +m

wait