module.exports = {
  moduleFileExtensions: ["ts", "tsx", "js"],
  transform: {
    "\\.(ts|tsx)$": "ts-jest"
  },
  testMatch: [
    "**/__tests__/**/*.(js|ts)?(x)",
    "**/?(*.)+(spec|test).(js|ts)?(x)"
  ],
  rootDir: "src/int-tests",
  testEnvironment: "node"
};
