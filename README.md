# Seed Data

Seed data should existin the db/ directory for postgres. This data should contain everything needed for a
working development environment and for every integration test to pass.

In the below examples, replace the values of these with your own:

- `PGPASSWORD` is your password
- `-U` is the username
- `-d` is the database
- `-h` is the hostname of the server

## Password on cli

pg cli comments don't have an inline password argument, but they read an environment variable. Use this to set
your password before running these commands.

```
export PGPASSWORD=docker
```

## Dump structure for seed data

```
pg_dump -U postgres -d postgres -h 127.0.0.1 -s -c > /mnt/d/Projects/crm/db/postgres/seed/struct.pgsql
```

## Dump data for seed data

```
pg_dump -U postgres -d postgres -h 127.0.0.1 -s -c > /mnt/d/Projects/crm/db/postgres/seed/struct.pgsql
```

## Restore seed strcture and data

```
psql -U postgres -d postgres -h 127.0.0.1 < /mnt/d/Projects/crm/db/postgres/seed/struct.pgsql
psql -U postgres -d postgres -h 127.0.0.1 < /mnt/d/Projects/crm/db/postgres/seed/data.pgsql
```
