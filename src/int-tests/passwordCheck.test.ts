import * as bcrypt from "bcrypt";

import { Logger } from "../logger";

describe("Password Check", () => {
  // const logger = new Logger();
  it(`bcrypt.compare should be able to match against the hash from bcrypt.hash`, async () => {
    const password = "password";
    const hash = await bcrypt.hash("password", 10);
    expect(bcrypt.compare(password, hash)).resolves.toBe(true);
  });
});

const logger = new Logger();
const go = async () => {
    const password = "password";
    const hash = await bcrypt.hash("password", 10);
    const result = await bcrypt.compare(password, hash);

    if(result) { return result; } else { throw new Error("Test failed: result is invalid"); }
};

go().then((result) => logger.log("Test result: ", result));
