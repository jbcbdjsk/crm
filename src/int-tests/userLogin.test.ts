import * as http from 'http';

import { Logger } from '../logger';

import { UserLoginTest } from './modules/UserLoginTest';

const logger = new Logger();

// test the login procedure
describe("User login", () => {
  it(`should log in successfully`, async () => {
    const agent = new http.Agent({keepAlive: true});
    const userLoginTest = new UserLoginTest(agent);
    await userLoginTest.postLogin({organizationKey: "demo",  nameLogin: "demo", password: "password"});
    const result = await userLoginTest.getLogin();
    expect(result).toHaveProperty("id");
  });
});
