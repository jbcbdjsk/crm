import * as http from "http";
import * as util from "util";

import { Logger } from "../logger";

import { CustomerTest } from "./modules/CustomerTest";
import { UserLoginTest } from "./modules/UserLoginTest";

describe("Customer Add/ Update / Delete", () => {
  let agent: http.Agent;
  let userLoginTest: UserLoginTest;
  let customerTest: CustomerTest;
  beforeEach(() => {
    agent = new http.Agent({ keepAlive: true });
    userLoginTest = new UserLoginTest(agent);
    customerTest = new CustomerTest(agent);
  });
  it(`should add a customer, fetch it, update it, then delete it`, async () => {
    const d = new Date();
    const keyUser = "MYUSER" + d.getTime().toString();
    await userLoginTest.postLogin({
      nameLogin: "demo",
      organizationKey: "demo",
      password: "password",
    });
    const result = await customerTest.addCustomer({
      addresses: [
        {
          city: "Jonesboro",
          isMailing: true,
          isPhysical: true,
          isPrimary: true,
          postalCode: "72401",
          state: "AR",
          street1: "123 Test Script St",
          street2: "",
        },
        {
          city: "Jonesboro",
          isMailing: true,
          isPhysical: false,
          isPrimary: false,
          postalCode: "72401",
          state: "AR",
          street1: "456 Test Script Mailing St",
          street2: "",
        },
      ],
      emailAddresses: [
        {
          emailAddress: "testscript1@example.com",
          isPrimary: true,
        },
        {
          emailAddress: "testscript2@example.com",
          isPrimary: false,
        },
      ],
      isBusiness: false,
      keyUser: "MYUSER" + d.getTime().toString(),
      nameFirst: "Demo",
      nameLast: "Customer",

      phones: [
        {
          isPrimary: true,
          phoneNumber: "8700000001",
          phoneType: "Home",
        },
        {
          isPrimary: false,
          phoneNumber: "8700000002",
          phoneType: "Mobile",
        },
      ],
    });
    expect(result).not.toBeNull();
    const customer = await customerTest.getCustomer(keyUser);

    customer.nameLast += " Updated";
    customer.addresses[0].street2 += "Updated line";

    await expect(customerTest.updateCustomer(customer)).resolves.not.toBeNull();
    await expect(customerTest.deleteCustomer(keyUser)).resolves.not.toBeNull();
  });
});
