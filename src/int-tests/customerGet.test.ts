import * as http from 'http';

import { CustomerTest } from './modules/CustomerTest';
import { UserLoginTest } from './modules/UserLoginTest';

    // {
    //     // test the login procedure
    //     let go = async () => {
    //         try {
    //             let result = false;
    //             let agent = new http.Agent({keepAlive: true});
    //             let customerTest = new CustomerTest(agent);
    //             try {
    //                 let customers = await customerTest.getCustomers();
    //                 console.log("Customers: ", util.inspect(customers, false, null));
    //             } catch(err) {
    //                 // this test should result in a 403 Forbidden response
    //                 if(err == 403) result = true;
    //             }
    //             return result;
    //         } catch(ex) {
    //             return false;
    //         }
    //     }

    //     let testName = `Try to get all customers without a valid login.
    //          \"Pass\" means that the attempt to get customers was forbidden";
    //     console.log("Begin test: " + testName)
    //     let result = await go();
    //     console.log("Test result", testName, result?"Pass":"Fail");
    // }

    // {
    //     // test the login procedure
    //     let go = async () => {
    //         try {
    //             let agent = new http.Agent({keepAlive: true});
    //             let userLoginTest = new UserLoginTest(agent);
    //             let customerTest = new CustomerTest(agent);
    //             await userLoginTest.postLogin(loginOptions);
    //             let customers = await customerTest.getCustomers();
    //             console.log(customers[0]);
    //             customerKey = customers[0].keyUser;
    //             console.log("Customers: ", util.inspect(customers, false, null));
    //             return Array.isArray(customers) && customers.length > 0;
    //         } catch(ex) {
    //             return false;
    //         }
    //     }

    //     let testName = "Get all Customers";
    //     console.log("Begin test: " + testName)
    //     let result = await go();
    //     console.log("Test result", testName, result?"Pass":"Fail")
    // }

describe("Customer", () => {
  const loginOptions = {organizationKey: "demo", nameLogin: "demo", password: "password"};
  let customerKey: string;
  let customerTest: CustomerTest;
  beforeEach(async () => {
    const agent = new http.Agent({keepAlive: true});
    const userLoginTest = new UserLoginTest(agent);
    customerTest = new CustomerTest(agent);
    await userLoginTest.postLogin(loginOptions);
  });
  it("should get a single customer", async () => {
    customerKey = 'MYUSER1531704103199';
    const customer = await customerTest.getCustomer(customerKey);
    expect(customer).toHaveProperty("id");
  });
});
