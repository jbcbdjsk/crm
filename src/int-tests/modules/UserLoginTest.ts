import * as http from 'http';
import * as request from 'request';

import config from './TestConfig';

export class UserLoginTest {
    public agent: http.Agent;
    constructor(agent: http.Agent) {
        this.agent = agent;
    }

    public postLogin(options: { organizationKey: string, nameLogin: string, password: string }) {
        return new Promise((resolve, reject) => {
            const req = request.post(`${config.url}/login`, {
                agent: this.agent,
                body: options,
                headers: {
                    'Content-type': 'application/json'
                },
                jar: true,
                json: true,
            }, (error, res, body) => {
                if(body) {
                    resolve(body);
                } else { reject("Failed to log in"); }
            });
        });
    }

    public getLogin() {
        return new Promise((resolve, reject) => {
            const req = request.get(`${config.url}/login`, {
                agent: this.agent,
                jar: true,
                json: true
            }, (error, res, body) => {
                if(body) { resolve(body); } else { reject("Failed to get log in"); }
            });
        });
    }
}
