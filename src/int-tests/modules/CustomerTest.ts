import * as http from 'http';
import * as request from 'request';

import config from './TestConfig';

export class CustomerTest {
  public agent: http.Agent;
  constructor(agent: http.Agent) {
    this.agent = agent;
  }

  public getCustomers() {
    return new Promise<any[]>((resolve, reject) => {
      const req = request.get(`${config.url}/customer`, {
        agent: this.agent,
        jar: true,
        json: true,
      }, (error, res, body) => {
        if(res.statusCode >= 400) { reject(res.statusCode); }
        if(body) {
          resolve(body);
        }
      });
    });
  }

  public getCustomer(keyUser) {
    return new Promise<any>((resolve, reject) => {
      const req = request.get(`${config.url}/customer/${keyUser}`, {
        agent: this.agent,
        jar: true,
        json: true,
      }, (error, res, body) => {
        if(res.statusCode >= 400) { reject(res.statusCode); }
        if(body) {
          resolve(body);
        }
      });
    });
  }

  public deleteCustomer(keyUser) {
    return new Promise<any>((resolve, reject) => {
      const req = request.delete(`${config.url}/customer/${keyUser}`, {
        agent: this.agent,
        jar: true,
        json: true,
      }, (error, res, body) => {
        if(res.statusCode >= 400) { reject(res.statusCode); }
        resolve(res.headers);
      });
    });
  }

  public updateCustomer(customerData) {
    return new Promise<http.IncomingHttpHeaders>((resolve, reject) => {
      const req = request.put(`${config.url}/customer/${customerData.keyUser}`, {
        agent: this.agent,
        body: customerData,
        jar: true,
        json: true,
      }, (error, res, body) => {
        if(res.statusCode >= 400) { reject(res.statusCode); }
        resolve(res.headers);
      });
    });
  }

  public addCustomer(customerData) {
    return new Promise<http.IncomingHttpHeaders>((resolve, reject) => {
      const req = request.post(`${config.url}/customer`, {
        agent: this.agent,
        body: customerData,
        jar: true,
        json: true,
      }, (error, res, body) => {
        if(res.statusCode >= 400) { reject(res.statusCode); }
        resolve(res.headers);
      });
    });
  }
}
