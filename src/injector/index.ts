// tslint:disable-next-line:interface-over-type-literal
type Newable = new (...args: any[]) => {};

export enum InjectVia {
  value,
  constructor,
  function
}
interface IType {
  name?: string;
  via: InjectVia;
}
export interface ITypes {
  [key: string]: IType;
}

/**
 * A dependency injector similar to inversify
 *
 * Create a types.ts file that exports an ITypes object
 * Create a config.ts file that imports your types list and call `injector.register()` to register types
 * Import your config.ts file early in your application
 * In tests, you can re-register types with mocks by importing this and calling `injector.register()` again
 * Use @injector.inject as a property decorator to inject the configured value on a class
 *
 */
class Injector {
  public static getInstance() {
    if (!Injector.injectorInstance) {
      Injector.injectorInstance = new Injector();
    }
    return Injector.injectorInstance;
  }
  private static injectorInstance: Injector;
  private instances: Map<IType | symbol, any> = new Map();
  private providers: Map<IType | symbol, () => any> = new Map();

  /**
   * Register or re-register a provider/value for a given type
   * @param type The type from configured types
   * @param value The provider/value to register
   */
  public register(type: IType | symbol, value, via: InjectVia = null) {
    let provider;
    this.instances.set(type, undefined); // allow reregistration
    via = via || this.getInjectorVia(type, value);
    const providerTypes = {
      [InjectVia.constructor]: () => new value(),
      [InjectVia.value]: () => value,
      [InjectVia.function]: () => value()
    };
    provider = providerTypes[via];
    this.providers.set(type, provider);
  }

  /**
   * Get the registered provided value of the desired type
   */
  public getValue(type: IType | symbol) {
    let inst = this.instances.get(type);
    if (!inst) {
      const provider = this.providers.get(type);
      if (!provider) {
        throw new Error(
          `No provider defined for type ${this.getTypeDescription(type) ||
            "unnamed"}`
        );
      }
      inst = provider();
      this.instances.set(type, inst);
    }
    return this.instances.get(type);
  }

  /**
   * Property decorator factory to register a property for dependency injection.  It doesn't directly inject
   * the property immediately. This replaces the property with a getter/setter that will use `injector.getValue()`
   * @param type A registered IType as a key from the this.providers Map
   */
  public inject(type: IType | symbol): PropertyDecorator {
    const injector = this;
    return (target: any, propertyKey: string | symbol) => {
      let propertyValue;
      Object.defineProperty(target, propertyKey, {
        get: () => {
          if (!propertyValue) {
            propertyValue = injector.getValue(type);
          }
          return propertyValue;
        },
        set: (val: any) => {
          propertyValue = val;
        }
      });
    };
  }

  private getInjectorVia(type: IType | symbol, value) {
    if (typeof type === "object" && type.via) {
      return type.via;
    }
    let via: InjectVia;
    if (typeof value === "function") {
      if (value.prototype) {
        via = InjectVia.constructor;
      } else {
        via = InjectVia.function;
      }
    } else {
      via = InjectVia.value;
    }
    return via;
  }

  private getTypeDescription = (type: IType | symbol) => {
    if (typeof type === "symbol") {
      return type.toString();
    } else {
      return type.name;
    }
  }
}

export default Injector.getInstance();
