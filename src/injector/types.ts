import { InjectVia } from "./";

const TYPES = {
  CanGetSessionHandler: Symbol("CanGetSessionHandler"),
  CanSetDBPASS: Symbol("CanSetDBPASS"),
  CustomerAddressService: Symbol("CustomerAddressService"),
  CustomerEmailAddressService: Symbol("CustomerEmailAddressService"),
  CustomerPhoneService: Symbol("CustomerPhoneService"),
  CustomerService: Symbol("CustomerService"),
  Logger: Symbol("Logger"),
  OrgUserService: Symbol("OrgUserService"),
  PgsqlServiceBase: Symbol("PgsqlServiceBase"),
  pg: Symbol("pg"),
};
export default TYPES;
