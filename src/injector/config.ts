import * as pg from "pg";

import { Logger } from "../logger";
import { CustomerAddressService } from "../services/pgsql/CustomerAddressService";
import { CustomerEmailAddressService } from "../services/pgsql/CustomerEmailAddressService";
import { CustomerPhoneService } from "../services/pgsql/CustomerPhoneService";
import { CustomerService } from "../services/pgsql/CustomerService";
import { OrgUserService } from "../services/pgsql/OrgUserService";
import { PgsqlServiceBase } from "../services/pgsql/PgsqlServiceBase";

import injector, { InjectVia } from "./";
import TYPES from "./types";

export const initInjector = () => {
  injector.register(TYPES.CustomerAddressService, CustomerAddressService);
  injector.register(TYPES.CustomerService, CustomerService);
  injector.register(TYPES.CustomerPhoneService, CustomerPhoneService);
  injector.register(TYPES.OrgUserService, OrgUserService);
  injector.register(
    TYPES.CustomerEmailAddressService,
    CustomerEmailAddressService,
  );
  injector.register(TYPES.Logger, Logger);
  injector.register(TYPES.PgsqlServiceBase, PgsqlServiceBase);
  injector.register(
    TYPES.CanGetSessionHandler,
    injector.getValue(TYPES.PgsqlServiceBase),
  );
  injector.register(
    TYPES.CanSetDBPASS,
    injector.getValue(TYPES.PgsqlServiceBase),
  );
  injector.register(TYPES.pg, pg, InjectVia.value);
  return injector;
};

initInjector();
