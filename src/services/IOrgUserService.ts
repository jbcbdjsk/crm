import { OrgUser } from "../models";

export interface IOrgUserService {
    getItems(options?: OrgUserServiceOptions): Promise<OrgUser[]>;
    getItem(options?: OrgUserServiceOptions): Promise<OrgUser>;
}

export class OrgUserServiceOptions {
    /** The id of the user to load */
    public id?: number = null;
    /** The id of the Organization to which the user must belong */
    public organizationId?: number = null;
    /** The login name of the user to load */
    public nameLogin?: string = null;
    /** The user key of the Organization to which the user must belong */
    public organizationKey?: string = null;
    /** How many records to load */
    public limit?: number = null;
}
