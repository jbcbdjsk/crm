import { initInjector } from "../../injector/config";

export class TestBase {
  public initInjector() {
    return initInjector();
  }
}
