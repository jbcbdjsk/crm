export class Pool {
  public mockConstructor = jest.fn();
  public connect = jest.fn(async () => {
    if (!this.client) {
      this.client = await new PoolClient();
    }
    return this.client;
  });
  public query = jest.fn(async (...args) => {
    if (!this.client) {
      this.client = await this.connect();
    }
    this.client.query(...args);
  });
  private client: PoolClient;
  constructor(options: any) {
    this.mockConstructor();
  }
}

// tslint:disable-next-line:max-classes-per-file
export class PoolClient {
  public query = jest.fn();
  public release = jest.fn();
}

export const types = {
  setTypeParser: jest.fn()
};
