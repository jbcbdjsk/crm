import { Customer } from '../models';

export interface ICustomerService {
    getItems(options?: CustomerServiceOptions): Promise<Customer[]>;

    getItem(options?: CustomerServiceOptions): Promise<Customer>;

    /**
     * Save a customer. It will use the same connection for all the queries, passing the connection to following methods
     * @param customer The customer to save
     * @returns Customer
     */
    saveItem(customer: Customer): Promise<Customer>;
}

export class CustomerServiceOptions {
    public id?: number;
    public organizationId: number;
    public limit?: number;
    public keyUser?: string;
    public includeDeleted?: boolean;
}
