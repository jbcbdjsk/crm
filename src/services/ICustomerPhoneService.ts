import { Customer, CustomerPhone } from "../models";

export interface ICustomerPhoneService {
    getItems(options?: CustomerPhoneServiceOptions): Promise<CustomerPhone[]>;
}

export class CustomerPhoneServiceOptions {
    public customer?: Customer;
    public customers?: Customer[];
}
