import { Customer, CustomerAddress } from '../models';

export interface ICustomerAddressService {
    getItems(options?: CustomerAddressServiceOptions): Promise<CustomerAddress[]>;
}

export class CustomerAddressServiceOptions {
    public customers?: Customer[];
    public customer?: Customer;
}
