'use strict';

export class Services {
  /**
   * This function is not meant to be called directly. This function is meant to be
   * replaced with a function of the same signature containing the database password
   * The database password should be read in via a secure method, such as an env var.
   * Making this a function keeps the password from being dumped out inadvertently.
   */
  public static DBPASS: () => string = () => "";
}
