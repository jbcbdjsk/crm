import { Customer, CustomerEmailAddress } from '../models';

export interface ICustomerEmailAddressService {
    getItems(options?: CustomerEmailAddressServiceOptions): Promise<CustomerEmailAddress[]>;

}

export class CustomerEmailAddressServiceOptions {
    public customers?: Customer[];
    public customer?: Customer;
}
