interface IParameterizer {
  (param: any): string;
  params: any[];
}

/**
 * Factory that returns a function that will mutate `params` by pushing each new `param` onto it
 * and returning a string `$${params.length}`
 *
 * @example
 *
 * const bar = "mybarval";
 * const baz = "mybazval";
 * const p = parameterizer();
 * const query = `SELECT * FROM foo WHERE bar = ${p(bar)} AND baz = ${p(baz)}`;
 * const result = db.query(query, params);
 * // results in
 * query === "SELECT * FROM foo WHERE bar = $1 AND baz = $2";
 * p.params === ["mybarval","mybazval"];
 */
export const parameterizer = () => {
  const params = [];
  const fn: IParameterizer = Object.assign(
    (param: any) => {
      params.push(param);
      return `$${params.length}`;
    },
    { params },
  );
  return fn;
};
