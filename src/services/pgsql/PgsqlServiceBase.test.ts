import { InjectVia } from "../../injector";
import TYPES from "../../injector/types";
import * as mockPg from "../test-helpers/mocks/pg";
import { TestBase } from "../test-helpers/TestBase";

import { PgsqlServiceBase } from "./PgsqlServiceBase";

describe("PgsqlServiceBase", () => {
  let testBase: TestBase;
  let serviceBase: PgsqlServiceBase;
  beforeEach(() => {
    const time2 = Date.now();
    testBase = new TestBase();
    const injector = testBase.initInjector();
    injector.register(TYPES.pg, mockPg, InjectVia.value);
    serviceBase = new PgsqlServiceBase();
  });
  describe("DBPASS", () => {
    it("should be a function that can be overridden with a new password", () => {
      expect(serviceBase.DBPASS()).toBe("");
    });
  });
  describe("getSessionHandler", () => {
    it("should get a session handler suitable for use with express session handling", () => {
      expect(typeof serviceBase.getSessionHandler()).toBe("function");
    });
  });
  describe("getDB", () => {
    it("should get an instance of the DB client, and the same pool should be re-used later", async () => {
      const client = await serviceBase.getDB();
      expect(client).toBeInstanceOf(mockPg.PoolClient);
      const sameClient = await serviceBase.getDB();
      expect(sameClient).toBe(client);
    });
  });
});
