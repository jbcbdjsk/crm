import * as connectPgSimple from "connect-pg-simple";
import * as express from "express";
import * as session from "express-session";
import * as pg from "pg";

import injector from "../../injector";
import TYPES from "../../injector/types";

const pgSession = connectPgSimple(session);

export interface IPgsqlServiceBase {
  DBPASS: () => string;
  getSessionHandler: () => express.RequestHandler;
  getDB: () => Promise<pg.PoolClient>;
}

export class PgsqlServiceBase implements IPgsqlServiceBase {
  @injector.inject(TYPES.pg) private pg: typeof pg;

  private pool: pg.Pool;
  public DBPASS: () => string = () => "";

  public getSessionHandler = () => {
    return session({
      cookie: { maxAge: 1 * 24 * 60 * 60 * 1000 },
      resave: false,
      saveUninitialized: true,
      secret: process.env.CRM_COOKIESECRET,
      store: new pgSession({
        pool: this.getPool(),
        tableName: "session",
      }),
    });
  }

  public getDB = async () => {
    const pool = this.getPool();
    return await pool.connect();
  }

  private getPool(): pg.Pool {
    if (!this.pool) {
      const pool = new this.pg.Pool({
        database: process.env.CRM_DBNAME,
        host: process.env.CRM_DBHOST,
        password: this.DBPASS(),
        user: process.env.CRM_DBUSER,
      });
      this.pool = pool;
    }
    const PGTYPE_INT = 20;
    /* istanbul ignore next */
    this.pg.types.setTypeParser(PGTYPE_INT, val => parseInt(val, 10));
    return this.pool;
  }
}
