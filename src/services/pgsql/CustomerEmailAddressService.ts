import { PoolClient } from "pg";

import injector from "../../injector";
import TYPES from "../../injector/types";
import { Customer, CustomerEmailAddress } from "../../models";
import { ICustomerEmailAddressService } from "../ICustomerEmailAddressService";

import { PgsqlServiceBase } from "./PgsqlServiceBase";

export class CustomerEmailAddressService
  implements ICustomerEmailAddressService {
  @injector.inject(TYPES.PgsqlServiceBase)
  private serviceBase: PgsqlServiceBase;
  public async getItems(
    options?: CustomerEmailAddressServiceOptions
  ): Promise<CustomerEmailAddress[]> {
    if (!options) {
      options = new CustomerEmailAddressServiceOptions();
    }
    let addresses: CustomerEmailAddress[];

    const db = await this.serviceBase.getDB();
    let query = `SELECT customerId as \"customerId\", keySort as \"keySort\", emailAddress as \"emailAddress\",
        isPrimary as \"isPrimary\" \n`;
    const params = [];
    query += "FROM CustomerEmailAddress \n";
    query += "WHERE TRUE \n";
    const x = options.customers.map(customer => customer.id);
    if (options.customers) {
      params.push([0].concat(options.customers.map(c => c.id)));
      query += "AND customerId = ANY ($" + params.length + "::INT[]) ";
    }
    if (options.customer) {
      params.push(options.customer.id);
      query += "AND customerId = $" + params.length;
    }
    query += "ORDER BY keySort";

    const { rows } = await db.query(query, params);
    db.release();

    addresses = rows.map(row => CustomerEmailAddress.fromObject(row));

    return addresses;
  }

  public async doSaveCustomer(
    customer: Customer,
    db: PoolClient
  ): Promise<void> {
    const promises = [];
    if (Array.isArray(customer.emailAddresses)) {
      let keySort = 1;
      for (const phone of customer.emailAddresses) {
        phone.customerId = customer.id;
        phone.keySort = keySort;
        promises.push(this.doSaveItem(phone, db));
        keySort++;
      }
      promises.push(
        this.doDeleteOtherItems(customer, customer.emailAddresses.length, db)
      );
    }
    await Promise.all(promises);
    return;
  }

  public async doSaveItem(
    emailAddress: CustomerEmailAddress,
    db: PoolClient
  ): Promise<void> {
    const params = [];
    const p = param => {
      params.push(param);
      return "$" + params.length;
    };
    const query = `
            INSERT INTO CustomerEmailAddress (customerId, keySort, emailAddress, isPrimary)
            VALUES(
                ${p(emailAddress.customerId)},
                ${p(emailAddress.keySort)},
                ${p(emailAddress.emailAddress)},
                COALESCE(${p(emailAddress.isPrimary)}, FALSE)
            )
            ON CONFLICT (customerId, keySort) DO UPDATE SET
            customerId = ${p(emailAddress.customerId)},
            keySort = ${p(emailAddress.keySort)},
            emailAddress = ${p(emailAddress.emailAddress)},
            isPrimary = COALESCE(${p(emailAddress.isPrimary)}, FALSE)
        `;

    const { rowCount } = await db.query(query, params);

    return;
  }

  private async doDeleteOtherItems(
    customer: Customer,
    minSort: number,
    db?: PoolClient
  ) {
    const params = [];
    const p = param => {
      params.push(param);
      return "$" + params.length;
    };
    const query = `
            DELETE FROM CustomerEmailAddress WHERE customerId = ${p(
              customer.id
            )} AND keySort > ${p(minSort)}
        `;
    try {
      await db.query(query, params);
    } catch (err) {
      throw new Error("CustomerEmailAddress delete error");
    }

    return;
  }
}

// tslint:disable-next-line:max-classes-per-file
export class CustomerEmailAddressServiceOptions {
  public customers?: Customer[];
  public customer?: Customer;
}
