import injector from "../../injector";
import TYPES from "../../injector/types";
import { OrgUser } from "../../models";
import { OrgUserServiceOptions } from "../IOrgUserService";

import { PgsqlServiceBase } from "./PgsqlServiceBase";

export class OrgUserService {
  @injector.inject(TYPES.PgsqlServiceBase)
  private serviceBase: PgsqlServiceBase;
  public async getItems(options?: OrgUserServiceOptions): Promise<OrgUser[]> {
    if (!options) {
      options = new OrgUserServiceOptions();
    }
    let users: OrgUser[] = [];
    const db = await this.serviceBase.getDB();
    const params: string[] = [];
    /** Add a ? to the query string and add the value the params array */
    const p = str => {
      params.push(str);
      return "$" + params.length;
    };

    const query = `
      SELECT id, organizationId as "organizationId", nameLast as "nameLast", nameFirst as "nameFirst",
      nameLogin as "nameLogin", passwordHash as "passwordHash"
      FROM OrgUser
      WHERE TRUE
      AND (${p(options.id)}::int IS NULL OR id = ${p(options.id)})
      AND (${p(options.organizationId)}::int IS NULL OR organizationId = ${p(
      options.organizationId
    )})
      AND (${p(options.nameLogin)}::varchar IS NULL OR nameLogin = ${p(
      options.nameLogin
    )})
      AND (${p(options.organizationKey)}::varchar IS NULL
          OR organizationId = (SELECT id FROM Organization WHERE keyUser = ${p(
            options.organizationKey
          )}))
      LIMIT ${options.limit || 10000}
    `;
    const { rows } = await db.query(query, params);
    db.release();

    users = rows.map(row => OrgUser.fromObject(row));

    return users;
  }

  public async getItem(options?: OrgUserServiceOptions): Promise<OrgUser> {
    if (!options) {
      options = new OrgUserServiceOptions();
    }
    options.limit = 1;
    let user: OrgUser;
    const users = await this.getItems(options);
    if (users.length > 0) {
      user = users[0];
    }
    return user;
  }
}
