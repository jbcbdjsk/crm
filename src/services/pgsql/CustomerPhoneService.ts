import { PoolClient } from "pg";

import injector from "../../injector";
import TYPES from "../../injector/types";
import { Customer, CustomerPhone } from "../../models";
import {
  CustomerPhoneServiceOptions,
  ICustomerPhoneService
} from "../ICustomerPhoneService";

import { PgsqlServiceBase } from "./PgsqlServiceBase";

export class CustomerPhoneService implements ICustomerPhoneService {
  @injector.inject(TYPES.PgsqlServiceBase)
  private serviceBase: PgsqlServiceBase;
  public async getItems(
    options?: CustomerPhoneServiceOptions
  ): Promise<CustomerPhone[]> {
    if (!options) {
      options = new CustomerPhoneServiceOptions();
    }
    let phones: CustomerPhone[];

    const db = await this.serviceBase.getDB();
    let query = `SELECT customerId as \"customerId\", keySort as \"keySort\", phoneNumber as \"phoneNumber\",
        phoneType as \"phoneType\", isPrimary as \"isPrimary\" \n`;
    const params = [];
    query += "FROM CustomerPhone \n";
    query += "WHERE TRUE \n";
    const x = options.customers.map(customer => customer.id);
    if (options.customers) {
      params.push([0].concat(options.customers.map(c => c.id)));
      query += "AND customerId = ANY ($" + params.length + "::INT[]) ";
    }
    if (options.customer) {
      params.push(options.customer.id);
      query += "AND customerId = $" + params.length;
    }
    query += "ORDER BY keySort";

    const { rows } = await db.query(query, params);
    db.release();
    phones = rows.map(row => CustomerPhone.fromObject(row));

    return phones;
  }

  /**
   * Insert/update all the records for a given customer, delete any that exist and not in the array
   */
  public async doSaveCustomer(
    customer: Customer,
    db: PoolClient
  ): Promise<void> {
    const promises = [];
    if (Array.isArray(customer.phones)) {
      let keySort = 1;
      for (const phone of customer.phones) {
        phone.customerId = customer.id; // force the customer id to match. don't trust the object itself
        phone.keySort = keySort; // force the keySort to be incremental. Don't trust the object itself
        promises.push(this.saveItem(phone, db));
        keySort++;
      }
      promises.push(
        this.doDeleteOtherItems(customer, customer.phones.length, db)
      );
    }
    await Promise.all(promises);
    return;
  }

  public async saveItem(
    customerPhone: CustomerPhone,
    db: PoolClient
  ): Promise<void> {
    const params = [];
    const p = param => {
      params.push(param);
      return "$" + params.length;
    };
    const query = `
            INSERT INTO CustomerPhone (customerId, keySort, phoneNumber, phoneType, isPrimary)
            VALUES (
                ${p(customerPhone.customerId)},
                ${p(customerPhone.keySort)},
                ${p(customerPhone.phoneNumber)},
                ${p(customerPhone.phoneType)},
                COALESCE(${p(customerPhone.isPrimary)}, FALSE)
            )
            ON CONFLICT (customerId, keySort) DO UPDATE SET
            customerId = ${p(customerPhone.customerId)},
            keySort = ${p(customerPhone.keySort)},
            phoneNumber = ${p(customerPhone.phoneNumber)},
            phoneType = ${p(customerPhone.phoneType)},
            isPrimary = COALESCE(${p(customerPhone.isPrimary)}, FALSE)
        `;
    const { rowCount } = await db.query(query, params);

    return;
  }

  private async doDeleteOtherItems(
    customer: Customer,
    minSort: number,
    db?: PoolClient
  ) {
    const params: string[] = [];
    const p = param => {
      params.push(param);
      return "$" + params.length;
    };
    const query = `
            DELETE FROM CustomerPhone WHERE customerId = ${p(
              customer.id
            )} AND keySort > ${p(customer.phones.length)}
        `;
    try {
      await db.query(query, params);
    } catch (err) {
      throw new Error("CustomerEmailAddress delete error");
    }
    return;
  }
}
