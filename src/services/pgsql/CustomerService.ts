import { PoolClient } from "pg";

import injector from "../../injector";
import TYPES from "../../injector/types";
import { Logger } from "../../logger";
import { Customer } from "../../models";
import { CustomerServiceOptions, ICustomerService } from "../ICustomerService";

import { CustomerAddressService } from "./CustomerAddressService";
import { CustomerEmailAddressService } from "./CustomerEmailAddressService";
import { CustomerPhoneService } from "./CustomerPhoneService";
import { parameterizer } from "./helpers";
import { PgsqlServiceBase } from "./PgsqlServiceBase";

export class CustomerService implements ICustomerService {
  @injector.inject(TYPES.CustomerAddressService)
  private customerAddressService: CustomerAddressService;
  @injector.inject(TYPES.CustomerPhoneService)
  private customerPhoneService: CustomerPhoneService;
  @injector.inject(TYPES.CustomerEmailAddressService)
  private customerEmailAddressService: CustomerEmailAddressService;
  @injector.inject(TYPES.Logger) private logger: Logger;
  @injector.inject(TYPES.PgsqlServiceBase)
  private serviceBase: PgsqlServiceBase;

  public async getItems(options?: CustomerServiceOptions): Promise<Customer[]> {
    if (!options) {
      options = new CustomerServiceOptions();
    }
    let customers: Customer[] = [];
    const db = await this.serviceBase.getDB();
    const p = parameterizer();

    const query = `
      SELECT id, organizationId as "organizationId", parentcustomerid as "parentCustomerId",
      keyUser as "keyUser", nameFirst as "nameFirst", nameLast as "nameLast", isBusiness as "isBusiness"
      FROM Customer
      WHERE TRUE
      AND (${p(options.id)}::int IS NULL OR id = ${p(options.id)})
      AND (${p(options.organizationId)}::int IS NULL
        OR organizationId = ${p(options.organizationId)}
      )
      AND (${p(options.keyUser)}::varchar IS NULL
        OR keyUser = ${p(options.keyUser)}
      )
      AND (isDeleted = false OR ${p(options.includeDeleted)})
      LIMIT ${options.limit || 10000}
    `;
    const { rows } = await db.query(query, p.params);
    db.release();
    customers = rows.map(row => Customer.fromObject(row));

    const addresses = await this.customerAddressService.getItems({ customers });
    const phones = await this.customerPhoneService.getItems({ customers });
    const emailAddresses = await this.customerEmailAddressService.getItems({
      customers,
    });
    customers.forEach(c => {
      c.addresses = addresses.filter(addr => addr.customerId === c.id);
      c.phones = phones.filter(phone => phone.customerId === c.id);
      c.emailAddresses = emailAddresses.filter(
        email => email.customerId === c.id,
      );
    });

    return customers;
  }

  public async getItem(options?: CustomerServiceOptions): Promise<Customer> {
    if (!options) {
      options = new CustomerServiceOptions();
    }
    options.limit = 1;
    let customer: Customer;
    const customers = await this.getItems(options);
    if (customers.length > 0) {
      customer = customers[0];
    }
    return customer;
  }

  /**
   * Save a customer. It will use the same connection for all the queries, passing the connection to following methods
   * @param customer The customer to save
   * @param txn Whether the function will create a db transaction that will roll back on error. Set to false if you
   * want to wrap this function in a larger txn.
   * @returns Customer
   */
  public async saveItem(customer: Customer): Promise<Customer> {
    const db = await this.serviceBase.getDB();

    try {
      await db.query("START TRANSACTION");

      await this.doSaveItem(customer, db);

      await Promise.all([
        this.customerAddressService.doSaveCustomer(customer, db),
        this.customerPhoneService.doSaveCustomer(customer, db),
        this.customerEmailAddressService.doSaveCustomer(customer, db),
      ]).catch(err => {
        this.logger.log(err);
        return Promise.reject(err);
      });

      await db.query("COMMIT");
    } catch (err) {
      db.query("ROLLBACK");
      throw err;
    } finally {
      db.release();
    }

    return customer;
  }

  public async doSaveItem(
    customer: Customer,
    db: PoolClient,
  ): Promise<Customer> {
    const p = parameterizer();
    let query = "";
    if (!customer.id) {
      query = `
        INSERT INTO Customer (keyUser, organizationId, parentcustomerId, nameFirst, nameLast, isBusiness, dateCreated,
        dateModified, isDeleted)
        VALUES (${p(customer.keyUser)}, ${p(customer.organizationId)},
        ${p(customer.parentCustomerId)}, ${p(customer.nameFirst)},
        ${p(customer.nameLast)}, ${p(customer.isBusiness)}, NOW(), NOW(), FALSE)
        RETURNING id
      `;
    } else {
      query = `
        UPDATE Customer
        SET keyUser = ${p(customer.keyUser)},
        nameFirst = ${p(customer.nameFirst)},
        nameLast = ${p(customer.nameLast)},
        isBusiness = ${p(customer.isBusiness)},
        dateModified = NOW(),
        isDeleted = COALESCE(${p(customer.isDeleted)}, FALSE)
        WHERE id = ${p(customer.id)}
        RETURNING id
      `;
    }

    const { rows } = await db.query(query, p.params);

    if (!customer.id) {
      customer.id = rows[0].id;
    }
    return customer;
  }
}
