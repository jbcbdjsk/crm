import { PoolClient } from 'pg';

import injector from '../../injector';
import TYPES from '../../injector/types';
import { Customer, CustomerAddress } from '../../models';
import { CustomerAddressServiceOptions, ICustomerAddressService } from '../ICustomerAddressService';

import { PgsqlServiceBase } from './PgsqlServiceBase';

export class CustomerAddressService implements ICustomerAddressService {
    @injector.inject(TYPES.PgsqlServiceBase) private serviceBase: PgsqlServiceBase;
    public async getItems(options?: CustomerAddressServiceOptions): Promise<CustomerAddress[]> {
        if (!options) { options = new CustomerAddressServiceOptions(); }
        let addresses: CustomerAddress[];

        const db = await this.serviceBase.getDB();
        let query = `SELECT customerId as \"customerId\", keySort as \"keySort\", street1, street2, city, state,
        postalCode as \"postalCode\", isPrimary as \"isPrimary\", isPhysical as \"isPrimary\",
        isMailing as \"isMailing\" \n`;
        const params = [];
        query += "FROM CustomerAddress \n";
        query += "WHERE TRUE \n";
        if (options.customers) {
            params.push([0].concat(options.customers.map((c) => c.id)));
            query += "AND customerId = ANY ($" + params.length + "::INT[]) ";
        }
        if (options.customer) { params.push(options.customer.id); query += "AND customerId = $" + params.length; }
        query += "ORDER BY keySort";

        const { rows, fields } = await db.query(query, params);
        db.release();

        addresses = rows.map((row) => CustomerAddress.fromObject(row));

        return addresses;
    }

    public async doSaveCustomer(customer: Customer, db: PoolClient): Promise<void> {
        const promises = [];
        if (Array.isArray(customer.addresses)) {
            let keySort = 1;
            for (const address of customer.addresses) {
                address.customerId = customer.id;
                address.keySort = keySort;
                promises.push(this.doSaveItem(address, db));
                keySort++;
            }
            promises.push(this.doDeleteOtherItems(customer, customer.addresses.length, db));
        }
        await Promise.all(promises);
        return;
    }

    public async doSaveItem(customerAddress: CustomerAddress, db?: PoolClient): Promise<void> {
        let rel = () => null;
        if (!db) { db = await this.serviceBase.getDB(); rel = () => db.release(); }

        const params = [];
        const p = (param) => { params.push(param); return "$" + params.length; };
        const query = `
            INSERT INTO CustomerAddress (customerId, keySort, street1, street2, city, state, postalCode,
                isPrimary, isPhysical, isMailing)
            VALUES (
                ${p(customerAddress.customerId)},
                ${p(customerAddress.keySort)},
                ${p(customerAddress.street1)},
                ${p(customerAddress.street2)},
                ${p(customerAddress.city)},
                ${p(customerAddress.state)},
                ${p(customerAddress.postalCode)},
                COALESCE(${p(customerAddress.isPrimary)}, FALSE),
                COALESCE(${p(customerAddress.isPhysical)}, FALSE),
                COALESCE(${p(customerAddress.isMailing)}, FALSE)
            )
            ON CONFLICT (customerId, keySort) DO UPDATE SET
            customerId = ${p(customerAddress.customerId)},
            keySort = ${p(customerAddress.keySort)},
            street1 = ${p(customerAddress.street1)},
            street2 = ${p(customerAddress.street2)},
            city = ${p(customerAddress.city)},
            state = ${p(customerAddress.state)},
            postalCode = ${p(customerAddress.postalCode)},
            isPrimary = COALESCE(${p(customerAddress.isPrimary)}, FALSE),
            isPhysical = COALESCE(${p(customerAddress.isPhysical)}, FALSE),
            isMailing = COALESCE(${p(customerAddress.isMailing)}, FALSE)
        `;

        const { rowCount } = await db.query(query, params);
        rel();

        return;
    }

    private async doDeleteOtherItems(customer: Customer, minSort: number, db?: PoolClient) {
        const params = [];
        const p = (param) => { params.push(param); return "$" + params.length; };
        const query = `
            DELETE FROM CustomerAddress WHERE customerId = ${p(customer.id)} AND keySort > ${p(minSort)}
        `;
        try {
            await db.query(query, params);
        } catch (err) { throw new Error("customeraddress delete other error"); }

        return true;
    }
}
