import { MySQL2Connection } from 'mysql2/promise';

import { Customer, CustomerAddress } from '../../models';
import { ICustomerAddressService } from '../ICustomerAddressService';

import { MysqlServiceBase } from './MysqlServiceBase';

export class CustomerAddressService implements ICustomerAddressService {
    public async getItems(options?: CustomerAddressServiceOptions): Promise<CustomerAddress[]> {
        if (!options) { options = new CustomerAddressServiceOptions(); }
        let addresses: CustomerAddress[];

        const db = await MysqlServiceBase.getDB();
        let query = `SELECT customerId, keySort, street1, street2, city, state, postalCode, isPrimary,
        isPhysical, isMailing \n`;
        const params = [];
        query += "FROM CustomerAddress \n";
        query += "WHERE TRUE \n";
        if (options.customers) {
            query += "AND customerId IN (?) ";
            params.push([0].concat(options.customers.map((c) => c.id)));
        }
        if (options.customer) { query += "AND customerId = ? "; params.push(options.customer.id); }
        query += "ORDER BY keySort";

        const [rows, fields] = await db.query(query, params);
        db.release();

        addresses = rows.map((row) => CustomerAddress.fromObject(row));

        return addresses;
    }

    public async saveCustomer(customer: Customer, db?: MySQL2Connection): Promise<void> {
        const promises = [];
        if(Array.isArray(customer.addresses)) {
            let keySort = 1;
            for(const address of customer.addresses) {
                address.customerId = customer.id;
                address.keySort = keySort;
                promises.push(this.saveItem(address, db));
                keySort++;
            }
            promises.push(this.deleteOtherItems(customer, customer.addresses.length, db));
        }
        await Promise.all(promises);
        return;
    }

    public async saveItem(customerAddress: CustomerAddress, db?: MySQL2Connection): Promise<void> {
        let rel = () => ({});
        if(!db) { db = await MysqlServiceBase.getDB(); rel = () => db.release(); }

        const params = [];
        const p = (param) => { params.push(param); return "?"; };
        const query = `
            REPLACE INTO CustomerAddress SET
            customerId = ${p(customerAddress.customerId)},
            keySort = ${p(customerAddress.keySort)},
            street1 = ${p(customerAddress.street1)},
            street2 = ${p(customerAddress.street2)},
            city = ${p(customerAddress.city)},
            state = ${p(customerAddress.state)},
            postalCode = ${p(customerAddress.postalCode)},
            isPrimary = ${p(customerAddress.isPrimary)},
            isPhysical = ${p(customerAddress.isPhysical)},
            isMailing = ${p(customerAddress.isMailing)}
        `;

        const [results, ...other] = await db.query(query, params);
        rel();

        return;
    }

    private async deleteOtherItems(customer: Customer, minSort: number, db?: MySQL2Connection) {
        let rel = () => null;
        if(!db) { db = await MysqlServiceBase.getDB(); rel = () => db.release(); }

        const params = [];
        const p = (param) => { params.push(param); return "?"; };
        const query = `
            DELETE FROM CustomerAddress WHERE customerId = ${p(customer.id)} AND keySort > ${p(minSort)}
        `;
        await db.query(query, params);
        rel();

        return true;
    }
}

// tslint:disable-next-line:max-classes-per-file
export class CustomerAddressServiceOptions {
    public customers?: Customer[];
    public customer?: Customer;
}
