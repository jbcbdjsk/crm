import { MySQL2Connection } from 'mysql2/promise';

import { Customer, CustomerEmailAddress } from '../../models';
import { ICustomerEmailAddressService } from '../ICustomerEmailAddressService';

import { MysqlServiceBase } from './MysqlServiceBase';

export class CustomerEmailAddressService implements ICustomerEmailAddressService {
    public async getItems(options?: CustomerEmailAddressServiceOptions): Promise<CustomerEmailAddress[]> {
        if (!options) { options = new CustomerEmailAddressServiceOptions(); }
        let addresses: CustomerEmailAddress[];

        const db = await MysqlServiceBase.getDB();
        let query = "SELECT customerId, keySort, emailAddress, isPrimary \n";
        const params = [];
        query += "FROM CustomerEmailAddress \n";
        query += "WHERE TRUE \n";
        const x = options.customers.map((customer) => customer.id);
        if (options.customers) { query += "AND customerId IN (0,?) "; params.push(options.customers.map((c) => c.id)); }
        if (options.customer) { query += "AND customerId = ? "; params.push(options.customer.id); }
        query += "ORDER BY keySort";

        const [rows, fields] = await db.query(query, params);
        db.release();

        addresses = rows.map((row) => CustomerEmailAddress.fromObject(row));

        return addresses;
    }

    public async saveCustomer(customer: Customer, db?: MySQL2Connection): Promise<void> {
        const promises = [];
        if(Array.isArray(customer.emailAddresses)) {
            let keySort = 1;
            for(const phone of customer.emailAddresses) {
                phone.customerId = customer.id;
                phone.keySort = keySort;
                promises.push(this.saveItem(phone, db));
                keySort++;
            }
            promises.push(this.deleteOtherItems(customer, customer.emailAddresses.length, db));
        }
        await Promise.all(promises);
        return;
    }

    public async saveItem(emailAddress: CustomerEmailAddress, db?: MySQL2Connection): Promise<void> {
        let rel = () => null;
        if(!db) { db = await MysqlServiceBase.getDB(); rel = () => db.release(); }
        const params = [];
        const p = (param) => { params.push(param); return "?"; };
        const query = `
            REPLACE INTO CustomerEmailAddress SET
            customerId = ${p(emailAddress.customerId)},
            keySort = ${p(emailAddress.keySort)},
            emailAddress = ${p(emailAddress.emailAddress)},
            isPrimary = ${p(emailAddress.isPrimary)}
        `;

        const [results, ...other] = await db.query(query, params);
        rel();

        return;
    }

    private async deleteOtherItems(customer: Customer, minSort: number, db?: MySQL2Connection) {
        let rel = () => null;
        if(!db) { db = await MysqlServiceBase.getDB(); rel = () => db.release(); }
        const params = [];
        const p = (param) => { params.push(param); return "?"; };
        const query = `
            DELETE FROM CustomerEmailAddress WHERE customerId = ${p(customer.id)} AND keySort > ${p(minSort)}
        `;
        await db.query(query, params);
        rel();

        return;
    }
}

// tslint:disable-next-line:max-classes-per-file
export class CustomerEmailAddressServiceOptions {
    public customers?: Customer[];
    public customer?: Customer;
}
