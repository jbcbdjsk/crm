import injector from "../../injector";
import TYPES from "../../injector/types";
import { Logger } from "../../logger";
import { Customer } from "../../models";
import { CustomerServiceOptions, ICustomerService } from "../ICustomerService";

import { CustomerAddressService } from "./CustomerAddressService";
import { CustomerEmailAddressService } from "./CustomerEmailAddressService";
import { CustomerPhoneService } from "./CustomerPhoneService";
import { MysqlServiceBase } from "./MysqlServiceBase";

export class CustomerService implements ICustomerService {
  @injector.inject(TYPES.CustomerAddressService)
  public customerAddressService: CustomerAddressService;
  @injector.inject(TYPES.CustomerPhoneService)
  public customerPhoneService: CustomerPhoneService;
  @injector.inject(TYPES.CustomerEmailAddressService)
  public customerEmailAddressService: CustomerEmailAddressService;
  @injector.inject(TYPES.Logger) private logger: Logger;

  public async getItems(options?: CustomerServiceOptions): Promise<Customer[]> {
    if (!options) {
      options = new CustomerServiceOptions();
    }
    let customers: Customer[] = [];
    const db = await MysqlServiceBase.getDB();
    const params: string[] = [];
    const p = param => {
      params.push(param);
      return "?";
    };

    const query = `
      SELECT id, organizationId, keyUser, nameFirst, nameLast, isBusiness
      FROM Customer
      WHERE TRUE
      AND (${p(options.id)} IS NULL OR id = ${p(options.id)})
      AND (${p(options.organizationId)} IS NULL OR organizationId = ${p(
      options.organizationId,
    )})
      AND (${p(options.keyUser)} IS NULL OR keyUser = ${p(options.keyUser)})
      AND (isDeleted = false OR ${p(options.includeDeleted)})
      LIMIT ${options.limit || 10000}
    `;
    const [rows, fields] = await db.query(query, params);
    db.release();
    customers = rows.map(row => Customer.fromObject(row));

    const addresses = await this.customerAddressService.getItems({ customers });
    const phones = await this.customerPhoneService.getItems({ customers });
    const emailAddresses = await this.customerEmailAddressService.getItems({
      customers,
    });
    customers.forEach(c => {
      c.addresses = addresses.filter(addr => addr.customerId === c.id);
      c.phones = phones.filter(phone => phone.customerId === c.id);
      c.emailAddresses = emailAddresses.filter(
        email => email.customerId === c.id,
      );
    });

    return customers;
  }

  public async getItem(options?: CustomerServiceOptions): Promise<Customer> {
    if (!options) {
      options = new CustomerServiceOptions();
    }
    options.limit = 1;
    let customer: Customer;
    const customers = await this.getItems(options);
    if (customers.length > 0) {
      customer = customers[0];
    }
    return customer;
  }

  /**
   * Save a customer. It will use the same connection for all the queries, passing the connection to following methods
   * @param customer The customer to save
   * @param txn Whether the function will create a db transaction that will roll back on error. Set to false if you
   * want to wrap this function in a larger txn.
   * @returns Customer
   */
  public async saveItem(
    customer: Customer,
    txn: boolean = true,
  ): Promise<Customer> {
    const db = await MysqlServiceBase.getDB();
    try {
      if (txn) {
        await db.query("START TRANSACTION");
      }
      const params: string[] = [];
      const p = param => {
        params.push(param);
        return "?";
      };
      let query = "";
      if (!customer.id) {
        query = `
          INSERT INTO Customer (keyUser, organizationId, nameFirst, nameLast, isBusiness, dateCreated,
            dateModified, isDeleted)
          VALUES (${p(customer.keyUser)}, ${p(customer.organizationId)}, ${p(
          customer.nameFirst,
        )},
          ${p(customer.nameLast)}, ${p(
          customer.isBusiness,
        )}, NOW(), NOW(), FALSE)
        `;
      } else {
        query = `
          UPDATE Customer
          SET keyUser = ${p(customer.keyUser)},
          nameFirst = ${p(customer.nameFirst)},
          nameLast = ${p(customer.nameLast)},
          isBusiness = ${p(customer.isBusiness)},
          dateModified = NOW(),
          isDeleted = IF(${p(customer.isDeleted)}, 1, 0)
          WHERE id = ${p(customer.id)}
        `;
      }
      const [results, ...other] = await db.query(query, params);

      if (!customer.id) {
        customer.id = results.insertId;
      }

      await Promise.all([
        this.customerAddressService.saveCustomer(customer, db),
        this.customerPhoneService.saveCustomer(customer, db),
        this.customerEmailAddressService.saveCustomer(customer, db),
      ]).catch(err => this.logger.log(err));

      if (txn) {
        await db.query("COMMIT");
      }
    } catch (err) {
      if (txn) {
        db.query("ROLLBACK");
      }
      throw err;
    } finally {
      db.release();
    }

    return customer;
  }
}
