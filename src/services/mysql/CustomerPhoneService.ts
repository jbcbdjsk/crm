import { MySQL2Connection } from 'mysql2/promise';

import { Customer, CustomerPhone } from '../../models';
import { CustomerPhoneServiceOptions, ICustomerPhoneService } from '../ICustomerPhoneService';

import { MysqlServiceBase } from './MysqlServiceBase';

export class CustomerPhoneService implements ICustomerPhoneService {
    public async getItems(options?: CustomerPhoneServiceOptions): Promise<CustomerPhone[]> {
        if(!options) { options = new CustomerPhoneServiceOptions(); }
        let phones: CustomerPhone[];

        const db = await MysqlServiceBase.getDB();
        let query  = "SELECT customerId, keySort, phoneNumber, phoneType, isPrimary \n";
        const params = [];
        query += "FROM CustomerPhone \n";
        query += "WHERE TRUE \n";
        const x = options.customers.map((customer) => customer.id);
        if (options.customers) { query += "AND customerId IN (0,?) "; params.push(options.customers.map((c) => c.id)); }
        if (options.customer) { query += "AND customerId = ? "; params.push(options.customer.id); }
        query += "ORDER BY keySort";

        const [rows, fields] = await db.query(query, params);
        db.release();
        phones = rows.map((row) => CustomerPhone.fromObject(row));

        return phones;
    }

    /**
     * Insert/update all the records for a given customer, delete any that exist and not in the array
     */
    public async saveCustomer(customer: Customer, db?: MySQL2Connection): Promise<void> {
        const promises = [];
        if(Array.isArray(customer.phones)) {
            let keySort = 1;
            for(const phone of customer.phones) {
                phone.customerId = customer.id; // force the customer id to match. don't trust the object itself
                phone.keySort = keySort; // force the keySort to be incremental. Don't trust the object itself
                promises.push(this.saveItem(phone, db));
                keySort++;
            }
            promises.push(this.deleteOtherItems(customer, customer.phones.length, db));
        }
        await Promise.all(promises);
        return;
    }

    public async saveItem(customerPhone: CustomerPhone, db?: MySQL2Connection): Promise<void> {
        let rel = () => null;
        if(!db) { db = await MysqlServiceBase.getDB(); rel = () => db.release(); }
        const params = [];
        const p = (param) => { params.push(param); return "?"; };
        const query = `
            REPLACE INTO CustomerPhone SET
            customerId = ${p(customerPhone.customerId)},
            keySort = ${p(customerPhone.keySort)},
            phoneNumber = ${p(customerPhone.phoneNumber)},
            phoneType = ${p(customerPhone.phoneType)},
            isPrimary = ${p(customerPhone.isPrimary)}
        `;
        const [results, ...other] = await db.query(query, params);
        rel();
        return;
    }

    private async deleteOtherItems(customer: Customer, minSort: number, db?: MySQL2Connection) {
        let rel = () => null;
        if(!db) { db = await MysqlServiceBase.getDB(); rel = () => db.release(); }
        const params: string[] = [];
        const p = (param) => { params.push(param); return "?"; };
        const query = `
            DELETE FROM CustomerPhone WHERE customerId = ${p(customer.id)} AND keySort > ${p(customer.phones.length)}
        `;
        await db.query(query, params);
        rel();
        return;
    }
}
