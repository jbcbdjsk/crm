import * as mysql from 'mysql2/promise';

export class MysqlServiceBase {
  public static DBPASS: ()=>string = () => "";

  /**
   * Get the DB connection to a server defined in the pool.
   */
  public static getPool(): mysql.MySQLPool {
    if(!MysqlServiceBase.pool) {
      MysqlServiceBase.pool = mysql.createPool({
        connectionLimit: 10,
        database: process.env.CRM_DBNAME,
        host: process.env.CRM_DBHOST,
        password: MysqlServiceBase.DBPASS(),
        typeCast: function castField( field, useDefaultTypeCasting ) {
          // We only want to cast bit fields that have a single-bit in them. If the field
          // has more than one bit, then we cannot assume it is supposed to be a Boolean.
          if ( ( field.type === "BIT" ) && ( field.length === 1 ) ) {
            const bytes = field.buffer();

            // A Buffer in Node represents a collection of 8-bit unsigned integers.
            // Therefore, our single "bit field" comes back as the bits '0000 0001',
            // which is equivalent to the number 1.
            return( bytes[ 0 ] === 1 );
          }
          if((field.type === "TINY") && (field.length === 1)) {
            const str = field.string();
            if( str === "1") {
              return true;
            } else if( str === "0") {
              return false;
            } else {
              return null;
            } // if it's not a 1 or 0 then it's probably null
          }

          return useDefaultTypeCasting();
        },
        user: process.env.CRM_DBUSER,
      });
    }
    return MysqlServiceBase.pool;
    }

  public static async getDB(): Promise<mysql.MySQL2Connection> {
    const pool = MysqlServiceBase.getPool();
    return await pool.getConnection();
  }
    private static pool: mysql.MySQLPool;
}

export { CustomerAddressService } from './CustomerAddressService';
export { CustomerService  } from './CustomerService';
export { OrgUserService } from './OrgUserService';
export { CustomerPhoneService } from './CustomerPhoneService';
export { CustomerEmailAddressService } from './CustomerEmailAddressService';
