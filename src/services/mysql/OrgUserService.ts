import { OrgUser } from '../../models';
import { OrgUserServiceOptions } from '../IOrgUserService';

import { MysqlServiceBase } from './MysqlServiceBase';

export class OrgUserService {
  public async getItems(options?: OrgUserServiceOptions): Promise<OrgUser[]> {
    if (!options) { options = new OrgUserServiceOptions(); }
    let users: OrgUser[] = [];
    const db = await MysqlServiceBase.getDB();
    const params: string[] = [];
    /** Add a ? to the query string and add the value the params array */
    const p = (str: string) => { params.push(str); return "?"; };
    const query = `
    SELECT id, organizationId, nameLast, nameFirst, nameLogin, passwordHash
    FROM OrgUser
    WHERE TRUE
    AND (${p(options.id.toString())} IS NULL OR id = ${p(options.id.toString())})
    AND (${p(options.organizationId.toString())} IS NULL
    OR organizationId = ${p(options.organizationId.toString())}
    )
    AND (${p(options.nameLogin)} IS NULL OR nameLogin = ${p(options.nameLogin)})
    AND (${p(options.organizationKey)} IS NULL OR organizationId = (
      SELECT id
      FROM Organization
      WHERE keyUser = ${p(options.organizationKey)}
      ))
      LIMIT ${options.limit||10000}
      `;
    const [rows] = await db.query(query, params);
    db.release();

    users = rows.map((row) => OrgUser.fromObject(row));

    return users;
  }

  public async getItem(options?: OrgUserServiceOptions): Promise<OrgUser> {
    if(!options) { options = new OrgUserServiceOptions(); }
    options.limit = 1;
    let user: OrgUser;
    const users = await this.getItems(options);
    if(users.length > 0) { user = users[0]; }
    return user;
  }
}
