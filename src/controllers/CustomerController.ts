import express from "express";

import injector from "../injector";
import TYPES from "../injector/types";
import { Customer, OrgUser } from "../models";
import {
  CustomerServiceOptions,
  ICustomerService,
} from "../services/ICustomerService";

import {
  IExpressController,
  IExpressControllerOpts,
  routable,
  route,
} from "./index";
import { OrgUserController } from "./OrgUserController";

/**
 * Class containing express routing methods for operations related to Customer objects
 */
@routable
export class CustomerController implements IExpressController {
  @injector.inject(TYPES.CustomerService)
  public customerService: ICustomerService;
  public app: express.Express;

  constructor(opts: IExpressControllerOpts) {
    this.app = opts.app;
  }

  /**
   * Get all customers
   */
  @route("get", "/customer")
  @OrgUserController.dCheckLogin()
  public async getItems(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) {
    const user: OrgUser = req.session.OrgUser;
    this.customerService
      .getItems({
        organizationId: user.organizationId,
      })
      .then(items => {
        // req.session.views = req.session.views ? req.session.views + 1 : 1;
        // res.send(req.session);
        res.send(items);
      }, next);
  }

  /**
   * Get a single customer by his keyUser value
   */
  @route("get", "/customer/:keyUser")
  @OrgUserController.dCheckLogin()
  public async getItem(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) {
    const user: OrgUser = req.session.OrgUser;
    try {
      const opt = new CustomerServiceOptions();
      opt.keyUser = req.params.keyUser.trim() as string;
      if (opt.keyUser.length === 0) {
        throw new Error("invalid key");
      }
      opt.organizationId = user.organizationId;
      this.customerService.getItem(opt).then(item => {
        if (item) {
          res.send(item);
        } else {
          res.sendStatus(404).send({ message: "Customer not found" });
        }
      }, next);
    } catch (err) {
      next(err);
    }
  }

  /**
   * Add a new customer via POST.
   *
   * On success: responds with HTTP status 201 and HTTP header Location to the new URL of the resource and no content
   * On failure: throw error passed to next()
   */
  @route("post", "/customer")
  @OrgUserController.dCheckLogin()
  public async addItem(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) {
    const user: OrgUser = req.session.OrgUser;
    try {
      const customer = Customer.fromObject(req.body);
      customer.organizationId = user.organizationId;
      customer.id = null;
      customer.validate();
      await this.customerService.saveItem(customer);
      res.setHeader("Location", `/customer/${customer.keyUser}`);
      res.sendStatus(201);
    } catch (err) {
      next(err);
    }
  }

  /**
   * Update an existing customer via PUT
   *
   * On success: responds with HTTP status 204 and no content
   * On failure: throw error passed to next()
   */
  @route("put", "/customer/:keyUser")
  @OrgUserController.dCheckLogin()
  public async updateItem(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) {
    const user: OrgUser = req.session.OrgUser;
    try {
      const customer = await this.customerService.getItem({
        keyUser: req.params.keyUser.trim() as string,
        organizationId: user.organizationId,
      });
      customer.updateObject(req.body);
      customer.validate();
      await this.customerService.saveItem(customer);
      res.sendStatus(204);
    } catch (err) {
      next(err);
    }
  }

  /**
   * Delete an existing customer. The customer will be soft-deleted by marking a flag in the database.
   *
   * On success: responds with HTTP status 204 and no content
   * On failure: throw error passed to next()
   */
  @route("delete", "/customer/:keyUser")
  @OrgUserController.dCheckLogin()
  public async deleteItem(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) {
    const user: OrgUser = req.session.OrgUser;
    try {
      const customer = await this.customerService.getItem({
        keyUser: req.params.keyUser.trim() as string,
        organizationId: user.organizationId,
      });
      customer.isDeleted = true;
      await this.customerService.saveItem(customer);
      res.sendStatus(204);
    } catch (err) {
      next(err);
    }
  }
}
