import * as express from 'express';

import { Controller } from './';

export class TestController {
    public app: express.Express;

    constructor(opts: any) {
        this.app = opts.app;

        this.getTest = this.getTest.bind(this);
        this.app.get("/test", this.getTest);
    }

    public async getTest(req: express.Request, res: express.Response, next: express.NextFunction) {
        res.send({ message: "Test bound by contructor!" });
    }
}
