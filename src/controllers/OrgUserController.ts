import express from "express";

import injector from "../injector";
import TYPES from "../injector/types";
import { OrgUser } from "../models";
import { IOrgUserService } from "../services/IOrgUserService";

import {
  IExpressController,
  IExpressControllerOpts,
  routable,
  route
} from "./index";

@routable
export class OrgUserController implements IExpressController {
  // async getItems (req: express.Request, res: express.Response, next: express.NextFunction) {
  //     this.customerService.getItems().then(items => {
  //         // req.session.views = req.session.views ? req.session.views + 1 : 1;
  //         // res.send(req.session);
  //         res.send(items);
  //     }, next);
  // }
  // async getItem (req: express.Request, res: express.Response, next: express.NextFunction) {
  //     try {
  //         let opt = new CustomerServiceOptions();
  //         opt.id = parseInt(req.params.id);
  //         if(!(opt.id > 0)) throw new Error("invalid id");
  //         this.customerService.getItem(opt).then(item => {
  //             let desc = Object.getOwnPropertyDescriptor(item, "customerAddressService");
  //             if(item) res.send(item);
  //             else {
  //                 console.log("Customer not found");
  //                 res.sendStatus(404).send({"message": "Customer not found"});
  //             }
  //         }, next)
  //     } catch(err) {
  //         next(err)
  //     }
  // }

  /**
   * Decorator factory to check the user login before allowing an exprss routing method to run.
   * Additional params to the factory may be added for speicific user checks
   */
  public static dCheckLogin() {
    return (
      target: any,
      propertyKey: string | symbol,
      descriptor: PropertyDescriptor
    ) => {
      const orig: (
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ) => any = descriptor.value;
      descriptor.value = function(
        req: express.Request,
        res: express.Response,
        next: express.NextFunction
      ) {
        const context = this;
        const args = arguments;
        const user = req.session.OrgUser;
        if (user) {
          orig.apply(context, args);
        } else {
          res.sendStatus(403).send({ message: "Not logged in" });
        }
      };
    };
  }

  @injector.inject(TYPES.OrgUserService) public orgUserService: IOrgUserService;
  public app: express.Express;

  constructor(opts: IExpressControllerOpts) {
    this.app = opts.app;
  }

  @route("post", "/login")
  public async postLogin(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let result: OrgUser;

    try {
      const user = await this.orgUserService.getItem({
        nameLogin: req.body.nameLogin,
        organizationKey: req.body.organizationKey
      });
      if (user) {
        if (await user.checkPassword(req.body.password)) {
          result = user;

          req.session.OrgUser = user;
        }
      }
    } catch (err) {
      next(err);
    }

    if (result) {
      res.send(result);
    } else {
      res.sendStatus(403).send({ message: "Invalid username or password" });
    }
  }

  @route("get", "/login")
  public async getLogin(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    const result = req.session.OrgUser;
    if (result) {
      res.send(result);
    } else {
      res.sendStatus(403).send({ message: "Not logged in" });
    }
  }
}
