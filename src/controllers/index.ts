export interface IExpressController {
  app: express.Express;
}
export class IExpressControllerOpts {
  public app: express.Express;
}

type Newable = new(...args: any[]) => {};

/**
 * Register a method to handle an express route
 *
 * @param method  The string http method, lowercase, that will be handled. This is like calling app[method](),
 * such as app.get(), app.post(), etc.
 * @param path The path that will be handled. This is like calling app.get(path)
 */
export function route(method: string, path: string) {
    return (target: any, propertyKey: string | symbol, descriptor: PropertyDescriptor) => {
        // similar to this express line:
        // app.get("/customer/:id", controllerInstance.getCustomer.bind(controllerInstance));
        if(!("routes" in target)) { target.routes = []; }
        target.routes.push([method, path, descriptor.value]);
    };
}

/**
 * Decorator to define a class as as routable, allowing its methods to use the @route decorator
 *
 * @param cls When called, the decorator will be passed the constructor of the class
 */
export function routable<T extends Newable>(cls: T) {
    // tslint:disable-next-line:max-classes-per-file
    return class extends cls {
        public routes: Array<[string, string, () => any]>;
        public app: express.Express;
        constructor(...args) {
            super(...args) ;
            this.routes.forEach(([reqMethod, path, fn]) => {
                this.app[reqMethod](path, fn.bind(this));
            });
        }
    };
}

import * as express from 'express';

import { CustomerController } from './CustomerController';
import { OrgUserController } from './OrgUserController';
import { TestController } from './TestController';

// tslint:disable-next-line:max-classes-per-file
export class Controller implements IExpressController {
    public testController: TestController;
    public customerController: CustomerController;
    public orgUserController: OrgUserController;

    public app: express.Express;
    /**
     * Calling this constructor will also instantiate the rest of the controllers in the application. Each controller
     * is responsible for setting up route handlers.
     * @param opts
     */
    constructor(opts: IExpressControllerOpts) {
        this.app = opts.app;

        this.customerController = new CustomerController(opts);
        this.testController = new TestController(opts);
        this.orgUserController = new OrgUserController(opts);
    }
}
