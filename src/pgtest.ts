import { Logger } from "./logger";
import { PgsqlServiceBase } from "./services/pgsql/PgsqlServiceBase";

const logger = new Logger();
PgsqlServiceBase.getDB().then(async (client)  => {
  const result = await client.query("SELECT * FROM customer WHERE id = $1", [1]);
  logger.log(result.rows);
}, (err) => logger.log(err));
