// use FastCGI instead of the HTTP module
import * as bodyParser from "body-parser";
import * as dotenv from "dotenv";
import * as express from "express";
import * as http from "http";
import * as fcgi from "node-fastcgi";

import { Controller } from "./controllers";
import injector from "./injector";
import "./injector/config";
import TYPES from "./injector/types";
import { PgsqlServiceBase } from "./services/pgsql/PgsqlServiceBase";

dotenv.config({ debug: true });
const logger = injector.getValue(TYPES.Logger);

if (!process.env.CRM_DBNAME) {
  throw new Error("CRM_DBNAME is not set. Missing environment config?");
}

if ("CRM_DBPASS" in process.env) {
  // this gets the DBPASS out of global vars immediately when the process starts
  // that means that dumping process.env will not dump the DBPASS
  // and setting it to a function means that dumping Model will also not dump the DBPASS, since it will dump [Function]
  // the var only exists as a let var local to this tiny block
  const DBPASS = process.env.CRM_DBPASS;
  injector.getValue(TYPES.CanSetDBPASS).DBPASS = () => DBPASS;
  delete process.env.CRM_DBPASS;
}

const app = express();

app.use(injector.getValue(TYPES.CanGetSessionHandler).getSessionHandler());

app.use(bodyParser.json());

const controllers = new Controller({ app });

app.all("*", (req, res, next) => {
  res.sendStatus(404);
});

app.use((err: Error, req, res: express.Response, next) => {
  logger.error(err.stack);
  if (!res.headersSent) {
    res.status(500).send("Something broke: " + err.message);
  }
});

process.stdout.on("error", err => {
  logger.log("This is the process.stdout error handler");
  logger.error(err);
});

if (process.env.SERVERFCGI) {
  fcgi.createServer(app).listen();
  logger.log("New FCGI process started");
} else {
  try {
    http.createServer(app).listen("8000");
  } catch (err) {
    logger.log(err);
    process.exit(); // can't continue if the port isn't open. If run via nodemon, it'll probably just retry
  }
  logger.log("New HTTP process started");
}

{
  process.on("beforeExit", () => {
    logger.log("Process is exiting");
  });
  process.on("disconnect", () => {
    logger.log("Process is disconnected");
  });
  // process.on("SIGTERM", () => {
  //     logger.log("Received SIGTERM");
  // });
  process.on("SIGINT", () => {
    logger.log("Received SIGINT");
    process.exit();
  });
  process.on("SIGHUP", () => {
    logger.log("Received SIGHUP");
    process.exit();
  });

  process.once("SIGUSR2", () => {
    logger.log("Received SIGUSR2");
    process.exit();
  });
}
