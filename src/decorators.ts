/**
 * Allows use of the @enumerable decorator factory on a class. This works by overriding the class constructor to modify
 * property descripters after the original constructor is called
 */
export function serializable<T extends new (...args: any[]) => {}>(cls: T) {
  return class extends cls {
    public enumerableProperties: Array<[string, boolean]>;
    constructor(...args) {
      super(...args);
      this.enumerableProperties.forEach(([prop, value]) => {
        const desc = Object.getOwnPropertyDescriptor(this, prop) || { writable: true, configurable: true };
        desc.enumerable = value;
        Object.defineProperty(this, prop, desc);
      });
    }
  };
}

/**
 * This decorator factory will define properties or methods as enumerable or not, meaning that adding
 * @enumerable(false) will prevent a property from
 * being serialized by JSON.stringify. This must be paired with adding @serializable to the class
 *
 * @see {serializable}
 */
export function enumerable(value: boolean): PropertyDecorator {
  return (target: any, name: string) => {
    if (!("enumerableProperties" in target)) { target.enumerableProperties = []; }
    target.enumerableProperties.push([name, value]);
  };
}

/**
 * A utility function to modify an Object to change whether a property is enumerable
 * @param target The object that will be modified
 * @param name The name of the property on the object that will be modified
 * @param value Whether the property will be enumerable.
 */
export function setEnumerable(target: any, name: string, value: boolean) {
  target[name] = target[name];
  const desc = Object.getOwnPropertyDescriptor(target, name) || {};
  if (desc.enumerable !== value) {
    desc.enumerable = value;
    Object.defineProperty(target, name, desc);
  }
}
