export class CustomerEmailAddress {

    public static fromObject(obj: any): CustomerEmailAddress {
        const address = new CustomerEmailAddress();
        if("customerId" in obj) { address.customerId = obj.customerId; }
        if("keySort" in obj) { address.keySort = obj.keySort; }
        if("emailAddress" in obj) { address.emailAddress  = obj.emailAddress; }
        if("isPrimary" in obj) { address.isPrimary = obj.isPrimary; }
        return address;
    }
    public customerId: number;
    public keySort: number;
    /** Email address */
    public emailAddress: string;
    public isPrimary: boolean;
}
