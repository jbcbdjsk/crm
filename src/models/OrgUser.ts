import * as bcrypt from "bcrypt";

import { enumerable, serializable } from '../decorators';

@serializable
export class OrgUser {

    public static fromObject(obj: any) {
        const item = new OrgUser();
        Object.assign(item, obj);
        return item;
    }
    public id: number;
    public organizationId: number;
    public nameFirst: string;
    public nameLast: string;
    public nameLogin: string;

    @enumerable(false) public passwordHash: string;
    @enumerable(false) public newPassword: string;

    public async checkPassword(pass) {
        if(!this.passwordHash) { return false; }
        return await bcrypt.compare(pass, this.passwordHash);
    }
}
