import {
  CustomerAddress,
  CustomerEmailAddress,
  CustomerPhone,
  ValidationError,
} from "./";

export class Customer {
  public static fromObject(obj: Partial<Customer>): Customer {
    const customer = new Customer();
    if ("id" in obj) {
      customer.id = obj.id;
    }
    if ("organizationId" in obj) {
      customer.organizationId = obj.organizationId;
    }
    if ("isDeleted" in obj) {
      customer.isDeleted = obj.isDeleted;
    }
    if ("parentCustomerId" in obj) {
      customer.parentCustomerId = obj.parentCustomerId;
    }
    customer.updateObject(obj);
    return customer;
  }

  public id: number;
  public organizationId: number;
  public parentCustomerId: number;
  public keyUser: string;
  public nameFirst: string;
  public nameLast: string;
  public isBusiness: boolean;
  public isDeleted: boolean;

  public addresses: CustomerAddress[];
  public emailAddresses: CustomerEmailAddress[];
  public phones: CustomerPhone[];

  /**
   * Check whether the properties of the object are set to valid values and whether the required fields are all set
   * This should be checked before performing database operations on the object.
   *
   * * User key is required and must be unique
   * * Last name is required. This is the business name if the Customer is a business
   * * isBusiness is required. It must be true or false.
   *
   * @throws {ValidationError} An array of error strings describing why the object is invalid
   * @returns true If no exception is thrown
   */
  public validate() {
    const errors: string[] = [];

    if (!(typeof this.keyUser === "string" && this.keyUser.length > 0)) {
      errors.push("User key is required");
    }
    if (!(typeof this.nameLast === "string" && this.nameLast.length > 0)) {
      errors.push("Last name is required");
    }
    if (this.isBusiness === undefined || this.isBusiness === null) {
      errors.push("Is Business is required");
    }
    if (Array.isArray(this.addresses) && this.addresses.length > 0) {
      if (!this.addresses.find(addr => addr.isPrimary)) {
        errors.push("If addresses are included, one must be primary");
      }
    }
    if (Array.isArray(this.emailAddresses) && this.emailAddresses.length > 0) {
      if (!this.emailAddresses.find(email => email.isPrimary)) {
        errors.push("If email addresses are included, one must be primary");
      }
    }
    if (Array.isArray(this.phones) && this.phones.length > 0) {
      if (!this.phones.find(phone => phone.isPrimary)) {
        errors.push("If phone numbers are included, one must be primary");
      }
    }
    if (errors.length) {
      throw new ValidationError("Customer invalid", errors);
    }
    return true;
  }

  public updateObject(obj: Partial<Customer>) {
    if ("keyUser" in obj) {
      this.keyUser = obj.keyUser;
    }
    this.keyUser = this.keyUser.replace(/[^a-zA-Z0-9_-]/, "");
    if ("nameFirst" in obj) {
      this.nameFirst = obj.nameFirst;
    }
    if ("nameLast" in obj) {
      this.nameLast = obj.nameLast;
    }
    if ("isBusiness" in obj) {
      this.isBusiness = obj.isBusiness;
    }
    if ("addresses" in obj && Array.isArray(obj.addresses)) {
      this.addresses = [];
      (obj.addresses as any[]).forEach(address =>
        this.addresses.push(CustomerAddress.fromObject(address)),
      );
      this.addresses.forEach((address, i) => (address.keySort = i + 1));
    }
    if ("emailAddresses" in obj && Array.isArray(obj.emailAddresses)) {
      this.emailAddresses = [];
      (obj.emailAddresses as any[]).forEach(emailAddress => {
        return this.emailAddresses.push(
          CustomerEmailAddress.fromObject(emailAddress),
        );
      });
      this.emailAddresses.forEach(
        (emailAddress, i) => (emailAddress.keySort = i + 1),
      );
    }
    if ("phones" in obj && Array.isArray(obj.phones)) {
      this.phones = [];
      (obj.phones as any[]).forEach(phone =>
        this.phones.push(CustomerPhone.fromObject(phone)),
      );
      this.phones.forEach((phone, i) => (phone.keySort = i + 1));
    }
  }
}
