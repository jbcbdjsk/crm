export class CustomerAddress {

    public static fromObject(obj: any): CustomerAddress {
        const address = new CustomerAddress();
        if("customerId" in obj) { address.customerId = obj.customerId; }
        if("keySort" in obj) { address.keySort = obj.keySort; }
        if("street1" in obj) { address.street1  = obj.street1; }
        if("street2" in obj) { address.street2 = obj.street2; }
        if("city" in obj) { address.city = obj.city; }
        if("state" in obj) { address.state = obj.state; }
        if("postalCode" in obj) { address.postalCode = obj.postalCode; }
        if("isPrimary" in obj) { address.isPrimary = obj.isPrimary; }
        if("isPhysical" in obj) { address.isPhysical = obj.isPhysical; }
        if("isMailing" in obj) { address.isMailing = obj.isMailing; }
        return address;
    }
    public customerId: number;
    public keySort: number;
    /** first street address line */
    public street1: string;
    /** section street address line */
    public street2: string;
    /** name of the city */
    public city: string;
    /** name of the state or province */
    public state: string;
    /** postal code (zip or other) */
    public postalCode: string;
    public isPrimary: boolean;
    public isPhysical: boolean;
    public isMailing: boolean;
}
