export enum QuantityUOM {
  each = "ea",
  hour = "hr",
}
export class QuantityProudctInfo {
  public costPerUnit: number;
  public quantity: number;
  public uom: QuantityUOM;
}
