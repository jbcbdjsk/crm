import { QuantityProudctInfo } from "./QuantityProduct";
import { ProductSubscriptionInfo } from "./SubscriptionProduct";

export class Product {
  public id: number;
  public name: string;
  public price: number;
  public subscriptionInfo: ProductSubscriptionInfo;
  public quantityInfo: QuantityProudctInfo;
}
