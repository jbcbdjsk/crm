import { PhoneType } from "./PhoneType";

export class CustomerPhone {

    public static fromObject(obj: CustomerPhone): CustomerPhone {
        const phone = new CustomerPhone();
        if("customerId" in obj) { phone.customerId = obj.customerId; }
        if("keySort" in obj) { phone.keySort = obj.keySort; }
        if("phoneNumber" in obj) { phone.phoneNumber = obj.phoneNumber; }
        if("phoneType" in obj) { phone.phoneType = obj.phoneType; }
        if("isPrimary" in obj) { phone.isPrimary = obj.isPrimary; }
        return phone;
    }
    public customerId: number;
    public keySort: number;
    public phoneNumber: string;
    public phoneType: PhoneType;
    public isPrimary: boolean;
}
