export enum PhoneType {
    Mobile = "Mobile",
    Office = "Office",
    Fax = "Fax",
    Home = "Home",
    None = ""
}
