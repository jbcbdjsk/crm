export class ValidationError extends Error {
    public errors: any[];
    constructor(message, errors) {
        super(message);
        this.errors = errors;
    }
}
