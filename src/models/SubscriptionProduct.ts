export enum DurationUOM {
  day = "day",
  month = "month",
  year = "year",
}
export class ProductSubscriptionInfo {
  public costPerUnit: number;
  public duration: number;
  public uom: DurationUOM;
  public renewalPrice: number;
}
