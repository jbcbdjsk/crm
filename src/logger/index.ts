// tslint:disable:no-console
export class Logger {
  public log = (...args) => console.log(...args);
  public warn = (...args) => console.warn(...args);
  public error = (...args) => console.error(...args);
}
