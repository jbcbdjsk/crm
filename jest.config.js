module.exports = {
  moduleFileExtensions: ["ts", "tsx", "js"],
  transform: {
    "\\.(ts|tsx)$": "ts-jest"
  },
  testMatch: [
    "**/__tests__/**/*.(js|ts)?(x)",
    "**/?(*.)+(spec|test).(js|ts)?(x)"
  ],
  testEnvironment: "node",
  testPathIgnorePatterns: ["/int-tests/", "/node_modules/"],
  rootDir: "src/",
  coverageDirectory: "./coverage"
};
